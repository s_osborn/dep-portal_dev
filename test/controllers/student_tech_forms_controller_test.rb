require 'test_helper'

class StudentTechFormsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student_tech_form = student_tech_forms(:one)
  end

  test "should get index" do
    get student_tech_forms_url
    assert_response :success
  end

  test "should get new" do
    get new_student_tech_form_url
    assert_response :success
  end

  test "should create student_tech_form" do
    assert_difference('StudentTechForm.count') do
      post student_tech_forms_url, params: { student_tech_form: { bringing_cell_phone: @student_tech_form.bringing_cell_phone, google_email_address: @student_tech_form.google_email_address, mobile_phone_can_sms: @student_tech_form.mobile_phone_can_sms, mobile_phone_has_data: @student_tech_form.mobile_phone_has_data, mobile_phone_number: @student_tech_form.mobile_phone_number } }
    end

    assert_redirected_to student_tech_form_url(StudentTechForm.last)
  end

  test "should show student_tech_form" do
    get student_tech_form_url(@student_tech_form)
    assert_response :success
  end

  test "should get edit" do
    get edit_student_tech_form_url(@student_tech_form)
    assert_response :success
  end

  test "should update student_tech_form" do
    patch student_tech_form_url(@student_tech_form), params: { student_tech_form: { bringing_cell_phone: @student_tech_form.bringing_cell_phone, google_email_address: @student_tech_form.google_email_address, mobile_phone_can_sms: @student_tech_form.mobile_phone_can_sms, mobile_phone_has_data: @student_tech_form.mobile_phone_has_data, mobile_phone_number: @student_tech_form.mobile_phone_number } }
    assert_redirected_to student_tech_form_url(@student_tech_form)
  end

  test "should destroy student_tech_form" do
    assert_difference('StudentTechForm.count', -1) do
      delete student_tech_form_url(@student_tech_form)
    end

    assert_redirected_to student_tech_forms_url
  end
end
