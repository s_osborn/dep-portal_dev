class CreateRequirements < ActiveRecord::Migration[5.0]
  def change
    create_table :requirements do |t|
      t.integer :requirement_def_id
      t.integer :user_id
      t.boolean :is_complete, :default => false #on the web
      t.boolean :is_exported, :default => false #into portico

      t.timestamps
    end
  end
end
