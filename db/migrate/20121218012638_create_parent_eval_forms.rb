class CreateParentEvalForms < ActiveRecord::Migration[5.0]
  def change
    create_table :parent_eval_forms do |t|
      t.integer :requirement_id

      t.text    :adjusting_homesickness
      t.text    :attend_before
      t.text    :concerns
      t.text    :describe_socially
      t.text    :ethnic_affiliation
      t.text    :ethnicity_drop_down
      t.text    :expectations
      t.text    :interests
      t.text    :general_information # currently St. Mark's only
      t.text    :learning_style
      t.boolean :takes_medications

      t.timestamps
    end
  end
end
