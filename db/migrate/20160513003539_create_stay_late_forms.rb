class CreateStayLateForms < ActiveRecord::Migration[5.0]
  def change
    create_table :stay_late_forms do |t|
      t.date :day
      t.boolean :stay_late, default: false
      t.boolean :overnight, default: false
      t.integer :user_id

      t.timestamps
    end
  end
end
