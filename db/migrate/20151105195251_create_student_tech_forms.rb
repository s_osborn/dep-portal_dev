class CreateStudentTechForms < ActiveRecord::Migration[5.0]
  def change
    create_table :student_tech_forms do |t|
      t.integer :requirement_id

      t.boolean :bringing_cell_phone
      t.string  :google_email_address
      t.string  :mobile_phone_number
      t.boolean :mobile_phone_can_sms
      t.boolean :mobile_phone_has_data
      t.boolean :will_rent_device

      t.timestamps
    end
  end
end
