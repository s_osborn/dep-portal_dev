class CreateAirlines < ActiveRecord::Migration[5.0]
  def change
    create_table :airlines do |t|
      t.string :icao
      t.string :short_name
      t.string :name
      t.string :callsign
      t.string :country
      t.text :url
      t.string :location

      t.timestamps
    end
  end
end
