class CreateDocusignUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :docusign_urls do |t|
      t.integer :profile_id
      t.integer :requirement_id
      t.string :docusign_envelope_id
      t.string :url

      t.timestamps
    end
  end
end
