class CreateRequirementDefs < ActiveRecord::Migration[5.0]
  def change
    create_table :requirement_defs do |t|
      t.string :action
      t.string :category, :default => "Required"
      t.string :docusign_template_id
      t.string :name
      t.string :requirement_type
      t.string :short_code #used to retrieve defs by name from the aether
      t.float  :sort_order

      t.timestamps
    end

    add_index :requirement_defs, :short_code, name: "index_requirement_defs_on_short_code"
  end

  def up
    add_attachment :requirement_defs, :attachment
  end

  def down
    remove_attachment :requirement_defs, :attachment
  end    
end
