class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|

      t.string :academic_title
      t.string :code
      t.string :fun_title
      t.integer :language_level
      t.integer :max_grade_level
      t.integer :min_grade_level
      t.string :period
      t.string :program_code
      t.string :sis_course_id
      t.string :course_type
      t.integer :year

      t.timestamps
    end
  end
end
