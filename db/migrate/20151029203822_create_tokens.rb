class CreateTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :tokens do |t|
      t.string :profile_id
      t.string :token
      t.timestamps
    end
  end
end
