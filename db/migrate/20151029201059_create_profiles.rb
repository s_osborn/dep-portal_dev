class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string  :email
      t.string  :full_name
      t.boolean :is_admin, default: false
      t.string  :password_digest

      t.timestamps
    end

    create_table :profiles_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :profile, index: true
    end
  end
end
