class CreateCourseForms < ActiveRecord::Migration[5.0]
  def change
    create_table :course_forms do |t|
      t.integer :requirement_id

      t.string :morning_choice
      t.string :p1_c1
      t.string :p1_c2
      t.string :p1_c3
      t.string :p1_c4
      t.string :p1_c5
      t.string :p1_c6
      t.string :p2_c1
      t.string :p2_c2
      t.string :p2_c3
      t.string :p2_c4
      t.string :p2_c5
      t.string :p2_c6
      t.string :p3_c1
      t.string :p3_c2
      t.string :p3_c3
      t.string :p3_c4
      t.string :p3_c5
      t.string :p3_c6
      t.string :p4_c1
      t.string :p4_c2
      t.string :p4_c3
      t.string :p4_c4
      t.string :p4_c5
      t.string :p4_c6
      t.string :princeton_review_selection
      t.string :session_1_wednesday_workshop
      t.string :session_1_weekend_1
      t.string :session_1_weekend_2
      t.string :session_2_wednesday_workshop
      t.string :session_2_weekend_1
      t.string :session_2_weekend_2
      t.string :w_c1
      t.string :w_c2
      t.string :w_c3
      t.string :w_c4
      t.string :w_c5
      t.string :w_c6
      t.string :wants_crew
      t.string :workshop_or_princeton_review

      t.timestamps
    end
  end
end
