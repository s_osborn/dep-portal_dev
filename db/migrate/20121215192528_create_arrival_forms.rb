class CreateArrivalForms < ActiveRecord::Migration[5.0]
  def change
    create_table :arrival_forms do |t|
      t.integer  :requirement_id

      t.string   :arrival_type
      t.text     :car_comment
      t.string   :car_how
      t.string   :car_how_other
      t.string   :car_service_name
      t.string   :plane_arrange_own_car_service_name
      t.string   :plane_arrange_own_car_service_contact_information
      t.string   :plane_arrange_own_to_explo
      t.text     :plane_arrange_own_to_explo_other
      t.date     :plane_arrival_date
      t.string   :plane_arrival_time
      t.string   :plane_arrival_time_am_pm
      t.string   :plane_flight_airline
      t.string   :plane_flight_airline_name
      t.string   :plane_flight_destination_airport
      t.string   :plane_flight_number
      t.string   :plane_how_get_to_explo
      t.text     :plane_how_get_to_explo_comments
      t.boolean  :plane_is_um
      t.string   :plane_ticket_full_name
      t.boolean  :plane_um_fee
      t.boolean  :plane_with_guardian
      t.string   :train_arrival_am_pm
      t.string   :train_arrival_time
      t.string   :train_number
      t.string   :train_origination_station
      t.string   :train_origination_town
      t.string   :train_ticket_confirmation
      #t.boolean  :colpitts
      #t.string   :plane_bradley_airport
      #t.string   :plane_flight_departure
      #t.string   :plane_connecting_airline
      #t.string   :plane_connecting_airport
      #t.string   :plane_connecting_departure_city
      #t.string   :plane_connecting_departure_time
      #t.string   :plane_connecting_departure_time_am_pm
      #t.string   :plane_connecting_number
      #t.string   :plane_flight_destination_airport_name
      #t.string   :plane_flight_destination_town
      #t.string   :plane_flight_origination_airport
      #t.string   :plane_flight_origination_town
      #t.boolean  :plane_is_direct
      #t.string   :sibling_program
      #t.string   :ticket_confirmation
      #t.string   :not_bradley_plane_non_standard_how
      #t.string   :not_bradley_plane_non_standard_other
      #t.text     :not_bradley_plane_non_standard_comment
      #t.string   :plane_non_standard_airport_name

      t.timestamps
    end
  end
end
