class CreateHousingForms < ActiveRecord::Migration[5.0]
  def change
    create_table :housing_forms do |t|
      t.integer :requirement_id

      t.text    :res_other_information
      t.string  :res_room_size
      t.string  :res_roommate_request_preference
      t.string  :res_roommate_request_name

      t.string  :day_group_request_name
      t.string  :day_group_request_preference
      t.text    :day_other_information

      t.timestamps
    end
  end
end
