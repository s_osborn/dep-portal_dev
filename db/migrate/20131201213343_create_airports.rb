class CreateAirports < ActiveRecord::Migration[5.0]
  def change
    create_table :airports do |t|
      t.string :icao
      t.string :latitude
      t.string :location
      t.string :longitude
      t.string :name
      t.string :timezone

      t.timestamps
    end
  end
end
