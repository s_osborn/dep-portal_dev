class CreateDepartureForms < ActiveRecord::Migration[5.0]
  def change
    create_table :departure_forms do |t|
      t.integer :requirement_id

      t.string   :departure_type
      t.string   :plane_airline
      t.string   :plane_airline_code
      t.date     :plane_departure_date
      t.string   :plane_departure_time
      t.string   :plane_departure_time_am_pm
      t.string   :plane_destination_airport
      t.string   :plane_flight_number
      t.string   :plane_force_how_getting_to_airport
      t.string   :plane_force_how_getting_to_airport_other
      t.text     :plane_force_how_getting_to_airport_comments
      t.string   :plane_how_getting_to_airport
      t.text     :plane_how_getting_to_airport_comments
      t.string   :plane_non_standard_airport_code
      t.string   :plane_non_standard_airport_name
      t.string   :plane_non_standard_airport_other
      t.boolean  :plane_um
      t.boolean  :plane_um_fee
      t.string   :plane_um_name
      t.boolean  :plane_with_guardian
      t.string   :plane_ticket_full_name
      t.string   :train_departure_time
      t.string   :train_departure_time_am_pm
      t.string   :train_destination_station
      t.string   :train_destination_town
      t.string   :train_number

      #t.boolean  :colpitts
      #t.string   :plane_connect_town
      #t.string   :plane_destination_town
      #t.boolean  :plane_is_direct
      #t.string   :plane_non_standard_airport_how
      #t.string   :plane_yale_airport
      #t.string   :ticket_confirmation

      t.timestamps
    end
  end
end
