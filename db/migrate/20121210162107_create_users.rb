class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string   :campdoc_profile_id
      t.string   :email
      t.string   :password_digest
      t.string   :portico_id
      t.string   :username
      t.text     :information

      t.timestamps
    end
  end
end
