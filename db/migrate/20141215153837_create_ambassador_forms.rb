class CreateAmbassadorForms < ActiveRecord::Migration[5.0]
  def change
    create_table :ambassador_forms do |t|
      t.integer :requirement_id

      t.text     :interest_in_leadership
      t.text     :favorite_moment
      t.string   :three_words_self
      t.string   :three_words_others
      t.string   :three_words_community
      t.text     :how_contribute
      t.text     :hope_to_gain
      t.text     :activity_idea
      t.text     :discussion_club_idea
      t.text     :main_event_idea
      t.boolean  :student_agreement
      t.boolean  :parent_agreement

      t.timestamps
    end
  end
end
