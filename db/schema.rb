# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160513003539) do

  create_table "airlines", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "icao"
    t.string   "short_name"
    t.string   "name"
    t.string   "callsign"
    t.string   "country"
    t.text     "url",        limit: 65535
    t.string   "location"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "airports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "icao"
    t.string   "latitude"
    t.string   "location"
    t.string   "longitude"
    t.string   "name"
    t.string   "timezone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ambassador_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.text     "interest_in_leadership", limit: 65535
    t.text     "favorite_moment",        limit: 65535
    t.string   "three_words_self"
    t.string   "three_words_others"
    t.string   "three_words_community"
    t.text     "how_contribute",         limit: 65535
    t.text     "hope_to_gain",           limit: 65535
    t.text     "activity_idea",          limit: 65535
    t.text     "discussion_club_idea",   limit: 65535
    t.text     "main_event_idea",        limit: 65535
    t.boolean  "student_agreement"
    t.boolean  "parent_agreement"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "arrival_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.string   "arrival_type"
    t.text     "car_comment",                                       limit: 65535
    t.string   "car_how"
    t.string   "car_how_other"
    t.string   "car_service_name"
    t.string   "plane_arrange_own_car_service_name"
    t.string   "plane_arrange_own_car_service_contact_information"
    t.string   "plane_arrange_own_to_explo"
    t.text     "plane_arrange_own_to_explo_other",                  limit: 65535
    t.date     "plane_arrival_date"
    t.string   "plane_arrival_time"
    t.string   "plane_arrival_time_am_pm"
    t.string   "plane_flight_airline"
    t.string   "plane_flight_airline_name"
    t.string   "plane_flight_destination_airport"
    t.string   "plane_flight_number"
    t.string   "plane_how_get_to_explo"
    t.text     "plane_how_get_to_explo_comments",                   limit: 65535
    t.boolean  "plane_is_um"
    t.string   "plane_ticket_full_name"
    t.boolean  "plane_um_fee"
    t.boolean  "plane_with_guardian"
    t.string   "train_arrival_am_pm"
    t.string   "train_arrival_time"
    t.string   "train_number"
    t.string   "train_origination_station"
    t.string   "train_origination_town"
    t.string   "train_ticket_confirmation"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
  end

  create_table "course_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.string   "morning_choice"
    t.string   "p1_c1"
    t.string   "p1_c2"
    t.string   "p1_c3"
    t.string   "p1_c4"
    t.string   "p1_c5"
    t.string   "p1_c6"
    t.string   "p2_c1"
    t.string   "p2_c2"
    t.string   "p2_c3"
    t.string   "p2_c4"
    t.string   "p2_c5"
    t.string   "p2_c6"
    t.string   "p3_c1"
    t.string   "p3_c2"
    t.string   "p3_c3"
    t.string   "p3_c4"
    t.string   "p3_c5"
    t.string   "p3_c6"
    t.string   "p4_c1"
    t.string   "p4_c2"
    t.string   "p4_c3"
    t.string   "p4_c4"
    t.string   "p4_c5"
    t.string   "p4_c6"
    t.string   "princeton_review_selection"
    t.string   "session_1_wednesday_workshop"
    t.string   "session_1_weekend_1"
    t.string   "session_1_weekend_2"
    t.string   "session_2_wednesday_workshop"
    t.string   "session_2_weekend_1"
    t.string   "session_2_weekend_2"
    t.string   "w_c1"
    t.string   "w_c2"
    t.string   "w_c3"
    t.string   "w_c4"
    t.string   "w_c5"
    t.string   "w_c6"
    t.string   "wants_crew"
    t.string   "workshop_or_princeton_review"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "courses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "academic_title"
    t.string   "code"
    t.string   "fun_title"
    t.integer  "language_level"
    t.integer  "max_grade_level"
    t.integer  "min_grade_level"
    t.string   "period"
    t.string   "program_code"
    t.string   "sis_course_id"
    t.string   "course_type"
    t.integer  "year"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "departure_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.string   "departure_type"
    t.string   "plane_airline"
    t.string   "plane_airline_code"
    t.date     "plane_departure_date"
    t.string   "plane_departure_time"
    t.string   "plane_departure_time_am_pm"
    t.string   "plane_destination_airport"
    t.string   "plane_flight_number"
    t.string   "plane_force_how_getting_to_airport"
    t.string   "plane_force_how_getting_to_airport_other"
    t.text     "plane_force_how_getting_to_airport_comments", limit: 65535
    t.string   "plane_how_getting_to_airport"
    t.text     "plane_how_getting_to_airport_comments",       limit: 65535
    t.string   "plane_non_standard_airport_code"
    t.string   "plane_non_standard_airport_name"
    t.string   "plane_non_standard_airport_other"
    t.boolean  "plane_um"
    t.boolean  "plane_um_fee"
    t.string   "plane_um_name"
    t.boolean  "plane_with_guardian"
    t.string   "plane_ticket_full_name"
    t.string   "train_departure_time"
    t.string   "train_departure_time_am_pm"
    t.string   "train_destination_station"
    t.string   "train_destination_town"
    t.string   "train_number"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  create_table "docusign_urls", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.integer  "requirement_id"
    t.string   "docusign_envelope_id"
    t.string   "url"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "housing_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.text     "res_other_information",           limit: 65535
    t.string   "res_room_size"
    t.string   "res_roommate_request_preference"
    t.string   "res_roommate_request_name"
    t.string   "day_group_request_name"
    t.string   "day_group_request_preference"
    t.text     "day_other_information",           limit: 65535
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "parent_eval_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.text     "adjusting_homesickness", limit: 65535
    t.text     "attend_before",          limit: 65535
    t.text     "concerns",               limit: 65535
    t.text     "describe_socially",      limit: 65535
    t.text     "ethnic_affiliation",     limit: 65535
    t.text     "ethnicity_drop_down",    limit: 65535
    t.text     "expectations",           limit: 65535
    t.text     "interests",              limit: 65535
    t.text     "general_information",    limit: 65535
    t.text     "learning_style",         limit: 65535
    t.boolean  "takes_medications"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "full_name"
    t.boolean  "is_admin",        default: false
    t.string   "password_digest"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "profiles_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "profile_id"
    t.index ["profile_id"], name: "index_profiles_users_on_profile_id", using: :btree
    t.index ["user_id"], name: "index_profiles_users_on_user_id", using: :btree
  end

  create_table "requirement_defs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "action"
    t.string   "category",                        default: "Required"
    t.string   "docusign_template_id"
    t.string   "name"
    t.string   "requirement_type"
    t.string   "short_code"
    t.float    "sort_order",           limit: 24
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["short_code"], name: "index_requirement_defs_on_short_code", using: :btree
  end

  create_table "requirements", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_def_id"
    t.integer  "user_id"
    t.boolean  "is_complete",        default: false
    t.boolean  "is_exported",        default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "stay_late_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date     "day"
    t.boolean  "stay_late",  default: false
    t.boolean  "overnight",  default: false
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "student_tech_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "requirement_id"
    t.boolean  "bringing_cell_phone"
    t.string   "google_email_address"
    t.string   "mobile_phone_number"
    t.boolean  "mobile_phone_can_sms"
    t.boolean  "mobile_phone_has_data"
    t.boolean  "will_rent_device"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "tokens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "profile_id"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "campdoc_profile_id"
    t.string   "email"
    t.string   "password_digest"
    t.string   "portico_id"
    t.string   "username"
    t.text     "information",        limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

end
