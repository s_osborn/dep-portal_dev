#==================================#

puts "Adding Requirement Definitions..."

course = RequirementDef.create({
  name: 'Course Selection',
  requirement_type: 'online',
  action: 'course_forms',
  short_code: 'POR.COURSEFORM',
  sort_order: 1
})
parent_eval = RequirementDef.create({
  name: 'Parent Evaluation',
  requirement_type: 'online',
  action: 'parent_eval_forms',
  short_code: 'POR.PARENTEVAL',
  sort_order: 2
})
parent_eval_mini = RequirementDef.create({
  name: 'Parent Evaluation (Explo Mini)',
  requirement_type: 'online',
  category: 'Optional',
  action: 'parent_eval_forms',
  short_code: 'POR.PARENTEVALMINI',
  sort_order: 2
})
housing_res = RequirementDef.create({
  name: 'Housing Information',
  requirement_type: 'online',
  action: 'housing_forms',
  short_code: 'POR.HOUSINGFORM',
  sort_order: 3
})
housing_day = RequirementDef.create({
  name: 'Day Group Form',
  requirement_type: 'online',
  action: 'housing_forms',
  short_code: 'POR.DAYGROUPFORM',
  sort_order: 3
})
arrival = RequirementDef.create({
  name: 'Residential Arrival Form',
  requirement_type: 'online',
  action: 'arrival_forms',
  short_code: 'POR.ARRIVALFORM',
  sort_order: 4
})
departure = RequirementDef.create({
  name: 'Residential Departure Form',
  requirement_type: 'online',
  action: 'departure_forms',
  short_code: 'POR.DEPARTUREFORM',
  sort_order: 5
})
departure = RequirementDef.create({
  name: 'Student Tech Form',
  requirement_type: 'online',
  action: 'student_tech_forms',
  short_code: 'POR.STUDENTTECH',
  sort_order: 6
})



RequirementDef.create({
  name: 'CampDoc health profile successfully submitted and reviewed<br><span class="emphasize">Once you have completed the CampDoc process, please allow two weeks for Explo to review and approve your health profile.</span>',
  requirement_type: 'information',
  category: 'Medical',
  short_code: 'POR.CAMPDOCCOMPLETE',
  sort_order: 1
})


authorized_visitors = RequirementDef.create({
  name: 'Authorized Visitors',
  requirement_type: 'pdf',
  category: 'Optional',
  short_code: 'POR.AUTHVIS',
  sort_order: 2
})

ambassador = RequirementDef.create({
  name: 'Explo Ambassador Application',
  requirement_type: 'online',
  action: 'ambassador_forms',
  category: 'Optional',
  short_code: 'POR.AMBASSADOR',
  sort_order: 6
})


#==================================#

# method to pull in a pile of fake users
# returns an array of users
def get_fake_users_from_tsv(file_name)
  fake_users = []
  fake_email = 'explo.email.test@gmail.com'
  fake_password = 'test'

	if File.exists?(file_name)
		puts "Importing fake users from '#{file_name}'..."

		data_file = File.open(file_name, "r+")
    line_number = 0
    keys = []

		data_file.each_with_index do |line, line_number|
			fields = line.gsub("\n",'').split("\t") #get fields from tab-separated file

			unless fields.empty?
        if line_number == 0
          # first line contains field names
          fields.each do |field|
            keys << field unless field.empty?
          end
        else
          # otherwise we're getting a list of users
          # username should be the first field in the spreadsheet
          unless fields[0].empty?
            fake_user = User.create({username:fields[0],email:fake_email,password:fake_password}) 
            puts "Added user #{fields[0]}"
            fields.each_with_index do |field, field_number|
              # everything but username is a "key" of a user
              fake_user.set_key(keys[field_number], field) unless (field.empty? || fake_user.nil?)
            end
            # we have to manually add forms for this user since they're not
            # getting added via the API. We do this after adding keys
            #fake_user.update_requirements
            fake_users << fake_user
          end
        end
			end
		end
	else
		puts "file '#{file_name}' doesn't exist."
	end
  return fake_users
end

# we're going to use the 'fake user filemaker module' instead
#fake_users = get_fake_users_from_tsv('lib/assets/fake_users.tsv')

#==================================#

def get_airlines(file_name)
	if File.exists?(file_name)
		puts "Importing airlines from '#{file_name}'..."

		data_file = File.open(file_name, "r+")
    line_number = 0

		data_file.each_with_index do |line, line_number|
			fields = line.gsub("\n",'').split("\t") #get fields from tab-separated file
      @callsign = fields[0]
      @country = fields[1]
      @icao = fields[2]
      @location = fields[3]
      @name = fields[4]
      @short_name = fields[5]
      @url = fields[6]

			unless fields.empty?
          Airline.create({
            callsign: @callsign,
            country: @country,
            icao: @icao,
            location: @location,
            name: @name,
            short_name: @short_name,
            url: @url
          }) 
          puts "Added airline #{@name}"
      end
		end
	else
		puts "file '#{file_name}' doesn't exist."
	end
end
get_airlines('lib/assets/airlines.tsv')


def get_airports(file_name)
	if File.exists?(file_name)
		puts "Importing airports from '#{file_name}'..."

		data_file = File.open(file_name, "r+")
    line_number = 0

		data_file.each_with_index do |line, line_number|
			fields = line.gsub("\n",'').split("\t") #get fields from tab-separated file
      @icao = fields[0]
      @latitude = fields[1]
      @location = fields[2]
      @longitude = fields[3]
      @name = fields[4]
      @timezone = fields[5]

			unless fields.empty?
          Airport.create({
            icao: @icao,
            latitude: @latitude,
            location: @location,
            longitude: @longitude,
            name: @name,
            timezone: @timezone
          }) 
          puts "Added airport #{@name}"
      end
		end
	else
		puts "file '#{file_name}' doesn't exist."
	end
end
get_airports('lib/assets/airports.tsv')

#==================================#
