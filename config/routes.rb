Rails.application.routes.draw do

  namespace 'admin' do
    resources 'user_search', only: [:index, :create]
  end
  scope module: 'admin' do
    resources :airlines do
      collection do
        get 'autocomplete_airline_name'
      end
    end
    resources :airports do
      get 'autocomplete_airport_name', :on => :collection
    end
    resources :requirement_defs
  end

  namespace 'campdoc' do
    resource :sso, only: [:create]
    resources :profiles, only: [:create, :destroy] do
      get 'completed'
      post 'destroy'
    end
  end
  
  resources :dashboard, only: [:index]
  namespace 'dashboard' do
    resources :payment, only: [:new]
    resources :summer_contact, only: [:index]
    resources :summer_faq, only: [:index]
    namespace 'transportation' do
      resources :carpool_list, only: [:index]
      resources :bus_list, only: [:index]
    end
  end

  namespace 'docusign' do
    resources :requirements, only: [:show, :destroy]
    resources :pdfs, only: [:show]
    resources :results, only: [:show]
  end

  namespace :flights do
    resource :lookup, only: [:create]
  end

  scope module: 'forms' do
    resources :arrival_forms, :only => [:edit, :show, :update]
    resources :ambassador_forms, :only => [:edit, :show, :update]
    resources :course_forms, :only => [:edit, :show, :update]
    resources :departure_forms, :only => [:edit, :show, :update]
    resources :housing_forms, :only => [:edit, :show, :update]
    resources :parent_eval_forms, :only => [:edit, :show, :update]
    resources :stay_late_forms, only: [:index, :update] do
      collection do
        get 'fm'
      end
    end
    resources :student_tech_forms, :only => [:edit, :show, :update]
  end

  resources :profiles do
    member do
      get 'login_as'
      get 'reset_password'
      get 'resend_welcome'
    end
  end

  resources :requirements, only: [:index, :update]
  resources :sessions, only: [:new, :create, :destroy]

  namespace :users do
    resources :profile_associations, only: [:new, :create]
  end
  resources :users, only: [:update, :destroy] do
    collection do
      post 'batch_update'
    end
  end

  get 'forms_home' => 'requirements#index', :as => :forms_home
  get 'forgot_password' => 'sessions#forgot_password', :as => :forgot_password
  post 'forgot_password' => 'sessions#forgot_password'
  get 'login' => 'sessions#new', :as => :login
  get 'login/:token' => 'sessions#create_from_email'
  get 'logout' => 'sessions#destroy', :as => :logout
  get 'reset_password(/:token)' => 'sessions#reset_password', :as => :reset_password
  post 'reset_password' => 'sessions#reset_password'
  get 'select_user' => 'sessions#select_user', :as => :select_user

  root :to => 'dashboard#index'
  #root :to => 'sessions#maintenance'
end
