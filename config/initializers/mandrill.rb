if Rails.env.production?
  mailer_config = File.open("#{Rails.root}/config/mailer.yml") 
  mailer_options = YAML.load(mailer_config) 
  ActionMailer::Base.smtp_settings = mailer_options 
  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.raise_delivery_errors = false
end
