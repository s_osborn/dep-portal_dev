namespace :deploy do
  desc "copy htaccess file from shared secret folder into public folder"
  task :copy_htaccess do
    on roles(:app) do
      execute "ln -sf #{shared_path}/secret/htaccess #{current_path}/public/.htaccess"
    end
  end

  desc "restart passenger"
  task :restart do
    on roles(:app) do
      execute "touch #{current_path}/tmp/restart.txt"
    end
  end

  after :deploy, 'deploy:copy_htaccess'
  after :deploy, 'deploy:restart'
end
