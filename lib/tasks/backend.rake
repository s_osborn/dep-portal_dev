namespace :admin do
  desc "Clear out any orphaned forms"
  task :clear_orphaned_forms => 'environment' do
    AmbassadorForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    ArrivalForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    CourseForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    DepartureForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    HousingForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    ParentEvalForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
    StudentTechForm.all.each do |f|
      unless f.requirement && f.requirement.user
        f.destroy
      end
    end
  end

  desc "DANGER - Clear out ALL forms"
  task :clear_all_forms => 'environment' do
    AmbassadorForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    ArrivalForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    CourseForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    DepartureForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    HousingForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    ParentEvalForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
    StudentTechForm.all.each do |f|
      r = f.requirement
      if r
        r.is_complete = false
        r.is_exported = false
        r.save
      end
      f.destroy
    end
  end
end

namespace :docusign do
  desc "Test DocuSign login + general functions"
  task :test => 'environment' do
    client = DocusignRest::Client.new
    # this should list all of the available docusign templates
    puts client.get_templates
  end

  desc "Update statuses of requirements based on date range (hard-coded for 2015)"
  task :update_status_date_range => 'environment' do
    client = DocusignRest::Client.new
    # this is a big hit, and takes a long time
    completed_forms = client.get_envelope_statuses(from_date: '09/01/2015', from_to_status: 'completed')
    completed_forms['envelopes'].each do |envelope|
      docusign_url = DocusignUrl.find_by_docusign_envelope_id(envelope['envelopeId'])
      if docusign_url
        req = docusign_url.requirement
        if req
          unless req.is_complete
            puts "updating requirement to complete: id: #{req.id} envelope_id: #{docusign_url.docusign_envelope_id}"
            req.is_complete = true
            req.save
          end
        end
      end
    end
  end
end

namespace :user do
  desc "EXPLO: Clear out any user security tokens > 5 days old"
  task :clear_expired_tokens => 'environment' do
    Token.all.each do |token|
      if (token.updated_at < (Time.now - 5.days))
        token.delete
      end
    end
  end

  desc "Clear out any submitted forms for REAL USERS DANGER"
  task :clear_user_submitted_forms => 'environment' do
    User.all.each do |user|
      puts "Removing requirements for #{user.get_key('first_name')} #{user.get_key('last_name')}"
      user.requirements.each do |req|
        req.arrival_form.delete if req.arrival_form
        req.course_form.delete if req.course_form
        req.departure_form.delete if req.departure_form
        req.housing_form.delete if req.housing_form
        req.parent_eval_form.delete if req.parent_eval_form
        req.ambassador_form.delete if req.ambassador_form
        req.student_tech_form.delete if req.student_tech_form
        
        req.is_complete = false
        req.save
      end
    end
  end

end

namespace :fakeusers do
  desc "Clean out any submitted forms from fake users"
  task :clear_fake_user_forms => 'environment' do
    # method to pull in a pile of fake users
    # returns an array of users
    fake_users = []
    fake_password = 'test'
    file_name = 'lib/assets/fake_users.tsv'

    if File.exists?(file_name)
      puts "Importing fake users from '#{file_name}'..."

      data_file = File.open(file_name, "r+")
      line_number = 0
      keys = []

      data_file.each_with_index do |line, line_number|
        fields = line.gsub("\n",'').split("\t") #get fields from tab-separated file

        unless fields.empty?
          if line_number == 0
            # first line contains field names
            fields.each do |field|
              keys << field unless field.empty?
            end
          else
            # otherwise we're getting a list of users
            # username should be the first field in the spreadsheet
            unless fields[1].empty?
              fake_user = User.where(username:fields[1]).first
              if fake_user
                puts "Removing requirements for #{fake_user.username}"
                fake_user.requirements.each do |req|
                  req.arrival_form.delete if req.arrival_form
                  req.course_form.delete if req.course_form
                  req.departure_form.delete if req.departure_form
                  req.housing_form.delete if req.housing_form
                  req.parent_eval_form.delete if req.parent_eval_form
                  
                  req.is_complete = false
                  req.save
                end
              end
            end
          end
        end
      end
    else
      puts "file '#{file_name}' doesn't exist."
    end
  end


  desc "Delete all fake users"
  task :delete_fake_users => 'environment' do
    # method to pull in a pile of fake users
    # returns an array of users
    fake_users = []
    fake_password = 'test'
    file_name = 'lib/assets/fake_users.tsv'

    if File.exists?(file_name)
      puts "Importing fake users from '#{file_name}'..."

      data_file = File.open(file_name, "r+")
      line_number = 0
      keys = []

      data_file.each_with_index do |line, line_number|
        fields = line.gsub("\n",'').split("\t") #get fields from tab-separated file

        unless fields.empty?
          if line_number == 0
            # first line contains field names
            fields.each do |field|
              keys << field unless field.empty?
            end
          else
            # otherwise we're getting a list of users
            # username should be the first field in the spreadsheet
            unless fields[0].empty?
              fake_user = User.where(username:fields[0]).first
              if fake_user
                puts "Deleting user #{fake_user.username}"
                fake_user.delete
              end
            end
          end
        end
      end
    else
      puts "file '#{file_name}' doesn't exist."
    end
  end

end
