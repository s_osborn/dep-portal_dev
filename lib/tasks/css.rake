namespace 'css' do
  desc "Get latest CSS from Explo CDN"
  task :fetch_latest do
    css_dir = Rails.root.join('app', 'assets', 'stylesheets', 'cdn')
    %w{explo explo_color explo_typography tachyons}.each do |file|
      system "cd '#{css_dir}' && curl http://cdn.lab.explo.org/css/#{file}.css -O"
    end
    main_css = File.read "#{css_dir}/explo.css"
    File.open "#{css_dir}/explo.css", "w" do |f|
      main_css.split("\n").each do |line|
        f.puts line.gsub(".min.css", ".css")
      end
    end 
  end
end
