namespace :sync do

  desc "get un-synched courses from portico via HIVE + update locally"
  task :portico_courses => 'environment' do
    atoms = HiveService.get_unseen_atoms('portico','course','update')

    unless atoms.nil?
      atoms.each do |atom|
        atom_data = JSON.parse(atom['data'])
        unless atom_data['sis_course_id'].nil?
          # here we have to convert the "universal" data a bit
          atom_data['academic_title'] = atom_data['title']
          atom_data['fun_title'] = atom_data['title']
          atom_data.delete 'title'

          course = Course.find_or_create_by(sis_course_id: atom_data['sis_course_id'])
          if course.update_attributes(atom_data)
            receipt = HiveService.post_receipt(atom['id'])
            if receipt.nil?
              Rails.logger.error("ERROR: HIVE COURSE SYNC: #{atom.to_json}") 
            else
              puts "RECEIVED: #{atom.inspect}"
            end
          end
        end
      end
    end
  end

end
