class FlightLookupService
  def self.schedule_lookup_arriving(airline_code, flight_number, year, month, day)
    FlightStats::ScheduledFlight.by_carrier_and_flight_number_arriving_on airline_code, flight_number, year, month, day
  end

  def self.schedule_lookup_departing(airline_code, flight_number, year, month, day)
    FlightStats::ScheduledFlight.by_carrier_and_flight_number_departing_on airline_code, flight_number, year, month, day
  end
end
