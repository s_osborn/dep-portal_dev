class HiveService
  APPLICATION_NAME = "portal_#{Rails.env}"
  HIVE_BASE_URL = "https://hive.lab.explo.org"
  HIVE_TOKEN = Rails.env == "development" ? "29d407c76d48b42edf94c0db0f6b2c82222622f" : "d943df3f1e049a70576f8b71a2a6bb7fb2b091a499425"

  # returns a hash representing all un-seen HIVE objects matching a given
  # application, context, and process
  def self.get_unseen_atoms(application, context=nil, process=nil)
    form_data = { 
      receipts: APPLICATION_NAME,
      application: application 
    }
    form_data.merge!(context: context) unless context.nil?
    form_data.merge!(process: process) unless process.nil?
    
    post "/atom_search.json", form_data
  end

  # given an activerecord object, convert it to a JSON representation + post
  # that to the HIVE system
  def self.post_atom(context, process, object)
    form_data = {
      application: APPLICATION_NAME,
      context: context,
      process: process,
      data: object.attributes
    }
    post "/atoms", form_data
  end

  # mark a particular atom as being received by the portal
  def self.post_receipt(atom_id)
    form_data = { application: APPLICATION_NAME }
    post "/atoms/#{atom_id}/receipts", form_data
  end

  # remove a given atom
  def self.delete(atom_id)
    form_data = { atom_id: atom_id }
    post "/atoms/#{atom_id}/deletion", form_data
  end


private

  # POST to a given HIVE URI, given an API-valid uri_string
  # returns either the (JSON) result of the API call, or nil if there was no result
  def self.post(uri_string, form_data=nil)
    form_data.merge!(token: HIVE_TOKEN)
    uri = URI(URI.escape("#{HIVE_BASE_URL}#{uri_string}"))

    Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
      request = Net::HTTP::Post.new uri.request_uri
      request['Content-Type'] = 'application/json'
      request.set_form_data form_data unless form_data.nil?

      response = http.request request # Net::HTTPResponse object
      result = nil

      begin
        result = JSON.parse(response.try(:body))
      rescue
        Rails.logger.error "HIVE service POST: JSON parse error: #{response.inspect}"
      end

      return result
    end
  end
end
