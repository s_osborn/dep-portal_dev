class CampdocService
  def initialize
    #TODO initialize these as environment variables instead of being hard-coded
    # donald's key
    @campdoc_auth_key = 'MTA3NzYzOmQ3MzNlMGVkLTQxMTUtNGJkMy1hY2IwLWE4YTU5MTNjODQ1ZA' 
    @campdoc_api_base = 'https://app.campdoc.com/api'
    #@campdoc_org_id = '78825' #explo API test
    @campdoc_org_id = '22440' #real Explo API
  end


  # Associate a Portal/CampDoc profile with a Portal/CampDoc user
  # Returns boolean indicating success
  def self.associate(portal_profile, portal_user)
    uri_string = "#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles/-#{user.try(:portico_id)}/users"

    given_name = portal_profile.full_name.split(' ')[0]
    family_name = portal_profile.full_name.split(' ')[1..-1].join(' ')
    form_data = {
        email: portal_profile.email,
        givenName: given_name,
        familyName: family_name
    }

    response_json = post uri_string, form_data

    result = response_json.nil? ? false : true
  end

  # Create a CampDoc profile, given an appropriate hash
  # For sample form data, see the "CampDoc::CampdocUsers::ProfileJSON" field in
  # Portico
  # Returns the profile ID of the created CampDoc profile, or nil if there was
  # a failure
  def self.create_campdoc_profile(profile_data_hash)
    response_json = post("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles", profile_data_hash)
    profile_id = response_json.try(:fetch, 'id', nil)
  end

  # De-associate a given CampDoc registration from a CampDoc profile
  def self.destroy_registration(campdoc_profile_id, campdoc_registration_id)
    response_json = delete("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles/#{campdoc_profile_id}/registrations/#{campdoc_registration_id}")
  end

  # Return ALL completed Campdoc profiles
  #     profile sample { "id": 81379, "identifier": "ZB5 8U45 6NO Z1F 7NI",
  #     "givenName": "Maria Luna", "middleName": "", "familyName": "Reale",
  #     "dob": "1996-03-05", "sex": "Female", "phase": "past", "completeness":
  #     null }
  def self.get_completed_campdoc_profiles
    # The response here should be a list of CampDoc profiles
    response_json = get("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles/all")
    
    completed_profiles = []
    response_json.try(:each) do |profile|
      if (profile['phase'] == 'present' || profile['phase'] == 'future') && profile['completeness'] == 100
        # add profile to the list of profiles to export
        completed_profiles << profile

        # set the requirement (temporarily) to "complete" for the user. It's
        # not "official" until you hear it from Portico, but this will make
        # it appear faster for families
        user = User.find_by(portico_id: profile['identifier'])
        user.add_requirement('POR.CAMPDOCCOMPLETE', 'Done') unless user.nil?
      end
    end

    return completed_profiles
  end

  # Get the CampDoc profile ID for a given Portal user
  # Returns a CampDoc profile ID, nil if there was a failure
  def self.get_campdoc_profile_id(portal_user)
    # The user's Portico ID is the unique profile identifier in CampDoc
    response_json = get("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles/-#{portal_user.try(:portico_id)}")
    campdoc_profile_id = result.try(:fetch, 'id', nil)
  end

  # Returns a hash of CampDoc registrations associated with a campdoc profile
  # ID (if any)
  def self.get_registrations(campdoc_profile_id)
    response_json = get("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/profiles/#{campdoc_profile_id}/registrations")
  end
  
  # Given a valid email from the CampDoc organization, grab a Single Sign-On
  # URL for it.
  # Returns the SSO URL.
  def self.get_sso_url(email)
    response_json = post("#{@campdoc_api_base}/organizations/#{@campdoc_org_id}/sso/#{email}")
    sso_url = response_json.try(:fetch, 'url', nil)
  end



  private

  def delete(uri_string, form_data=nil)
    # Attempt to find any existing profile, based on Portico Contact ID
    uri = URI(URI.escape(uri_string))
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
      request = Net::HTTP::Delete.new uri.request_uri
      request['Content-Type'] = 'application/json'
      request['Authorization'] = "Basic #{@campdoc_auth_key}"
      request.set_form_data form_data unless form_data.nil?

      response = http.request request
      result = nil

      if response.body == "Forbidden"
        logger.error "CampdocService DELETE: forbidden: #{uri_string}"
      else
        begin
          result = JSON.parse(response.try(:body))
        rescue
          logger.error "CampdocService DELETE: JSON parse error: #{response.inspect}"
        end
      end

      return result
    end
  end

  def get(uri_string, form_data=nil)
    # Attempt to find any existing profile, based on Portico Contact ID
    uri = URI(URI.escape(uri_string))
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
      request = Net::HTTP::Get.new uri.request_uri
      request['Content-Type'] = 'application/json'
      request['Authorization'] = "Basic #{@campdoc_auth_key}"
      request.set_form_data form_data unless form_data.nil?

      response = http.request request
      result = nil

      if response.body == "Forbidden"
        logger.error "CampdocService GET: forbidden: #{uri_string}"
      else
        begin
          result = JSON.parse(response.try(:body))
        rescue
          logger.error "CampdocService GET: JSON parse error: #{response.inspect}"
        end
      end

      return result
    end
  end
  
  # POST to a given CampDoc URI, given an API-valid uri_string
  # returns either the (JSON) result of the API call, or nil if there was no result
  def post(uri_string, form_data=nil)
    uri = URI(URI.escape(uri_string))
    Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
      request = Net::HTTP::Post.new uri.request_uri
      request['Content-Type'] = 'application/json'
      request['Authorization'] = "Basic #{@campdoc_auth_key}"
      request.set_form_data form_data unless form_data.nil?

      response = http.request request # Net::HTTPResponse object
      result = nil

      if response.body == "Forbidden"
        logger.error "CampdocService POST: forbidden: #{uri_string}"
      else
        begin
          result = JSON.parse(response.try(:body))
        rescue
          logger.error "CampdocService POST: JSON parse error: #{response.inspect}"
        end
      end

      return result
    end
  end

end
