class DocusignService
  # create a docusign form template, given a requirement + user
  # returns the URL of the created form AND the envelope response
  def self.create_form_url(profile, user, requirement, return_url)
    # grab the particular docusign form
    envelope_response = DocusignRest::Client.new.create_envelope_from_template(
      status: 'sent',
      email: {
        subject: requirement.requirement_def.name,
        body: "Please fill out this form"
      },
      template_id: requirement.requirement_def.docusign_template_id,
      signers: [
        {
          embedded: true,
          name: profile.try(:full_name),
          email: profile.try(:email),
          role_name: 'Legal Guardian',
          # pre-fill any tabs you might want to fill
          text_tabs: [
            {
              label: 'Student Name',
              name: 'Student Name',
              value: "#{user.get_key('first_name')} #{user.get_key('last_name')}"
            }
          ]
        }
      ]
    )

    Rails.logger.info "DOCUSIGN: generating URL with callback: #{return_url}, #{envelope_response.inspect}"

    # this will return the URL from the form we just created
    url = get_form_url(DocusignUrl.new(docusign_envelope_id: envelope_response.try(:fetch, 'envelopeId', nil)), profile, return_url)
    return url, envelope_response
  end

  # Download the PDF related to a given docusignUrl
  def self.download_pdf(docusign_url, file_path)
    DocusignRest::Client.new.get_combined_document_from_envelope(
        envelope_id: docusign_url.docusign_envelope_id,
        local_save_path: file_path
    )
  end

  # given a DocuSignURL record, return the URL associated with it
  def self.get_form_url(docusign_url, profile, return_url)
    response = DocusignRest::Client.new.get_recipient_view(
        envelope_id: docusign_url.docusign_envelope_id,
        name: "#{profile.try(:full_name)}",
        email: "#{profile.try(:email)}",
        return_url: return_url
      )
    if response['url'] == nil
      Rails.logger.error "DOCUSIGN: get_form_url error: #{response.inspect}"
      return nil
    else
      return response['url']
    end
  end

end
