# Utility class to express an Explo Session without having a database model
# for it
class ExploSession
  attr_accessor :campus
  attr_accessor :key
  attr_accessor :name
  attr_accessor :program
  attr_accessor :user_array

  def initialize(name="", key="", campus="", program="")
    self.name = name
    self.key = key
    self.campus = campus
    self.program = program
    self.user_array = []
  end

  def is_mini
    self.key.match(/w/)
  end

  def to_s
    "ExploSession: #{self.key} | #{self.name} | #{self.campus} | #{self.program}"
  end
end
