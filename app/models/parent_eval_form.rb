class ParentEvalForm < ActiveRecord::Base
  attr_accessor :form_type
  belongs_to :requirement

  # Non-mini form validations
  validates_presence_of :general_information, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 1.'
  validates_presence_of :adjusting_homesickness, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 2.'
  validates_presence_of :describe_socially, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 3.'
  validates_presence_of :interests, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 4.'
  validates_presence_of :learning_style, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 5.'
  validates_presence_of :attend_before, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 6.'
  validates_presence_of :expectations, 
    :if => :form_is_non_mini,
    :message => 'Please answer question 7.'
  validates_inclusion_of :takes_medications, 
    :in => [true, false],
    :if => :form_is_non_mini,
    :message => 'Please answer question 8.'

  # Mini form validations
  validates_presence_of :general_information, 
    :if => :form_is_mini,
    :message => 'Please answer question 1.'
  validates_presence_of :describe_socially, 
    :if => :form_is_mini,
    :message => 'Please answer question 2.'
  validates_presence_of :expectations, 
    :if => :form_is_mini,
    :message => 'Please answer question 3.'


  def form_is_mini
    form_type == 'mini'
  end

  def form_is_non_mini
    form_type == 'non_mini'
  end
end
