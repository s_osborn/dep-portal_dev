class CourseForm < ActiveRecord::Base
  #Virtual attributes
  attr_accessor :campus, :is_fluent, :is_session_one, :is_session_two

  belongs_to :requirement

  validate :yale_fields_are_correct
  validate :wellesley_fields_are_correct
  validate :wheaton_fields_are_correct

  def wheaton_fields_are_correct
    if campus == 'Wheaton College'
      # low language level students are ESOL
      if is_fluent
        check_no_course_choice_dupes(1, 3, 'course', 'week')
        check_no_course_choice_dupes(2, 3, 'course', 'week')
        check_no_course_choice_dupes(3, 3, 'course', 'week')
      end
      check_no_course_choice_dupes('w', 6, 'workshop')
    end
  end

  def wellesley_fields_are_correct
    if campus == 'Wellesley College'
      # low language level students are ESOL
      if is_fluent
        check_no_course_choice_dupes(1, 6)
        check_no_course_choice_dupes(2, 6)
      end
      check_no_course_choice_dupes('w', 6, 'workshop')
    end
  end

  def yale_fields_are_correct
    if campus == 'Yale University'
      # Morning course options
      check_no_course_choice_dupes(1, 6)
      check_no_course_choice_dupes(2, 6)

      # Afternoon academics
      errors.add(:workshop_or_princeton_review, 'Please select an afternoon academic option.') if workshop_or_princeton_review.nil?
      if workshop_or_princeton_review == 'workshops' || workshop_or_princeton_review == 'both'
        check_no_course_choice_dupes(3, 6, 'Explo at Yale workshop')
        check_no_course_choice_dupes(4, 6, 'Explo at Yale workshop')
      end
      if workshop_or_princeton_review == 'princeton_review' || workshop_or_princeton_review == 'both'
        errors.add(:princeton_review_selection, 'Please select a Princeton Review option') if princeton_review_selection.nil?
      end

      # Session 1 students only
      if is_session_one
        errors.add(:session_1_wednesday_workshop, 'Please select a first session Second Wednesdays option.') if session_1_wednesday_workshop.nil?
      end

      # Session 2 students only
      if is_session_two
        errors.add(:session_2_wednesday_workshop, 'Please select a second session Second Wednesdays option.') if session_2_wednesday_workshop.nil?
      end

    end
  end

  private

  # run through a series of (usually six) course choices and make sure none of them match
  def check_no_course_choice_dupes(period, num_choices=6, type='course', period_name='period')
    all_choices = []
    already_used = []
    prefix = 'p' if  type == 'course' || type == 'Explo at Yale workshop'
    period_description = type == 'workshop' ? '' : " for #{period_name} #{period}"
    1.upto num_choices do |i|
      period_name = "#{prefix}#{period}_c#{i}"
      choice = eval period_name
      if choice.nil? || choice.empty?
        errors.add(period_name, "Please select #{type} choice #{i}#{period_description}.")
      else
        all_choices << choice
        if (all_choices - already_used).empty?
          errors.add(period_name, "You may not request a #{type} code #{choice} multiple times in the same period.") 
        else
          already_used << choice
        end
      end
    end
  end

end
