class User < ActiveRecord::Base
  has_and_belongs_to_many :profiles
  has_many :requirements, :dependent => :destroy
  has_many :requirement_defs, :through => :requirements
  has_many :stay_late_forms, :dependent => :destroy

  # Add (or update) a single requirement (form) using its shortcode
  def add_requirement(shortcode, status, mod_on=nil)
    if status.match(/done/i) || status.match(/suppressed/i) || status.match(/ignore/i)
      is_complete = true 
    else
      is_complete = false 
    end

    @requirement_def = RequirementDef.find_by(short_code: shortcode)
    if @requirement_def
      @existing_requirement = self.requirements.find_by(requirement_def_id: @requirement_def.id)
      if @existing_requirement
        # passed information is assumed to be newer
        passed_requirement_is_newer = true
        # ... unless we've sent a modification date which is older than the
        # existing requirement
        if mod_on
          mod_on = Time.parse mod_on
          if mod_on < @existing_requirement.updated_at
            passed_requirement_is_newer = false
          end
        end
        # update the req if we've got newer information
        if passed_requirement_is_newer
          @existing_requirement.is_complete = is_complete 
          # if we're changing a requirement back to "false", make sure we're
          # also setting it to be not-exported
          @existing_requirement.is_exported = false if is_complete == false
          @existing_requirement.save
        end
      else
        self.requirements.create(
          requirement_def_id: @requirement_def.id,
          is_complete: is_complete,
        ) 
      end
    end
  end

  # Convenience function to see whether a key contains a value
  def contains(key, value)
    (self.get_key(key) =~ value) != nil
  end
   
  # return whether this user is enrolled in a given ExploSession/string
  def enrolled_in(explo_session, campus = nil)
    self.enrolled_sessions.each do |my_session|
      if explo_session.public_methods.include? :key
        # it is an ExploSession (see class def above)
        return true if (my_session.key == explo_session.key && my_session.campus == explo_session.campus)
      else
        # it is a string
        if (campus)
          return true if (my_session.key == explo_session && my_session.campus == campus)
        else
          return true if (my_session.key == explo_session)
        end
      end
    end
    return false
  end

  # returns a series of session representation objects
  def enrolled_sessions
    sessions = []
    sessions << ExploSession.new('1st Session', 's1', self.get_key('s1_campus'), self.get_key('s1_program_name')) if self.get_key('s1_arrival_date')
    sessions << ExploSession.new('Week 1', 'w1', "Wheaton College", self.get_key('w1_program_name')) if self.get_key('w1_arrival_date')
    sessions << ExploSession.new('Week 2', 'w2', "Wheaton College", self.get_key('w2_program_name')) if self.get_key('w2_arrival_date')
    sessions << ExploSession.new('Week 3', 'w3', "Wheaton College", self.get_key('w3_program_name')) if self.get_key('w3_arrival_date')
    sessions << ExploSession.new('2nd Session', 's2', self.get_key('s2_campus'), self.get_key('s2_program_name')) if self.get_key('s2_arrival_date')
    sessions << ExploSession.new('Week 4', 'w4', "Wheaton College", self.get_key('w4_program_name')) if self.get_key('w4_arrival_date')
    sessions << ExploSession.new('Week 5', 'w5', "Wheaton College", self.get_key('w5_program_name')) if self.get_key('w5_arrival_date')
    sessions << ExploSession.new('Week 6', 'w6', "Wheaton College", self.get_key('w6_program_name')) if self.get_key('w6_arrival_date')
    sessions
  end

  # utility method to return text that varies on the gender
  def gendered_text(male_value, female_value, other_value)
    if self.match('gender', 'M')
      male_value
    elsif self.match('gender', 'F')
      female_value
    else
      other_value
    end
  end

  # Retrieve just the value of a particular key of this user
  def get_key(key)
    retrieve_information
    user_key = @information[key]
  end

  # Given a set of keys to choose from, pick the first that matches
  def get_first_key(keys=[])
    keys.each do |key|
      result = self.get_key(key)
      return result if result
    end
  end

  # Convenience function to match a key with a value
  def match(key, value)
    self.get_key(key).strip.downcase == value.strip.downcase unless self.get_key(key).nil? || value.nil?
  end

  # Return the Date on which the user will be arriving
  def session_arrival_date
    if self.get_key('s1_arrival_date')
      session_arrival = self.get_key('s1_arrival_date')
    elsif self.get_key('s2_arrival_date')
      session_arrival = self.get_key('s2_arrival_date')
    elsif self.get_key('w1_arrival_date')
      session_arrival = self.get_key('w1_arrival_date')
    elsif self.get_key('w2_arrival_date')
      session_arrival = self.get_key('w2_arrival_date')
    elsif self.get_key('w3_arrival_date')
      session_arrival = self.get_key('w3_arrival_date')
    elsif self.get_key('w4_arrival_date')
      session_arrival = self.get_key('w4_arrival_date')
    elsif self.get_key('w5_arrival_date')
      session_arrival = self.get_key('w5_arrival_date')
    elsif self.get_key('w6_arrival_date')
      session_arrival = self.get_key('w6_arrival_date')
    else
      session_arrival = nil
    end
    session_arrival.to_date if session_arrival
  end

  # Return the Date on which the user will be departing (360 programs)
  def session_departure_date
    if self.get_key('s2_departure_date')
      session_departure = self.get_key('s2_departure_date')
    elsif self.get_key('w6_departure_date')
      session_departure = self.get_key('w6_departure_date')
    elsif self.get_key('w5_departure_date')
      session_departure = self.get_key('w5_departure_date')
    elsif self.get_key('w4_departure_date')
      session_departure = self.get_key('w4_departure_date')
    elsif self.get_key('s1_departure_date')
      session_departure = self.get_key('s1_departure_date')
    elsif self.get_key('w3_departure_date')
      session_departure = self.get_key('w3_departure_date')
    elsif self.get_key('w2_departure_date')
      session_departure = self.get_key('w2_departure_date')
    elsif self.get_key('w1_departure_date')
      session_departure = self.get_key('w1_departure_date')
    else
      session_departure = nil
    end
    session_departure.to_date if session_departure
  end

  # Set a piece of information about this user (a key)
  def set_key(key, value)
    # attempt to find the key
    #user_key = self.user_keys.find_by_key(key)
    retrieve_information
    user_key = @information[key]
    
    if user_key && !value.nil? && !value.empty?
      # re-set the key's value if it's found
      #user_key.value = value
      #user_key.save
      @information[key] = value
    elsif value.nil? || value.empty?
      # remove the key if the passed value is empty
      #remove_key key
      @information.delete key
    else
      # create the key from scratch if it didn't exist
      #self.user_keys.create!({:key => key, :value => value})
      @information[key] = value
    end

    save_information
  end

  # Remove a piece of information about the user (a key)
  def remove_key(key)
    retrieve_information
    information.delete key
    save_information
  end

  # remove a single requirement (form) using its shortcode
  def remove_requirement(shortcode)
    #requirement_def = RequirementDef.find_by_short_code(shortcode)
    my_def = self.requirement_defs.find_by_short_code(shortcode)
    if my_def
      requirement = self.requirements.find_by_requirement_def_id my_def.id
      requirement.destroy
    end
  end


  private

  def retrieve_information
    if self.information
      @information = JSON.parse self.information || {}
    else
      @information = {}
    end
  end

  def save_information
    self.information = @information.to_json
    self.save
  end
end
