class Notifier < ActionMailer::Base
  default :from => "portal@explo.org"

  # The initial email that gets sent to users
  def add_profile(profile, user, originating_name)
    @profile = profile
    @user = user
    @originating_name = originating_name

    mail( 
      :to => @profile.email,
      :from => "Explo Admissions <portal@explo.org>",
      :reply_to => @user.get_key('program_email'),
      :subject => "Explo Portal: #{@profile.email} added for #{@user.get_key('first_name')} #{@user.get_key('last_name')}"
    )
  end

  # Information on how to reset your password
  def forgot_password(profile, token)
    @profile = profile
    @token = token
    @program_email = @profile.users.first.get_key('program_email')

    mail( 
      :to => @profile.email,
      :from => "Explo Admissions <portal@explo.org>",
      :reply_to => @program_email,
      :subject => "Explo Portal: Password Reset"
    )
  end

  # The initial email that gets sent to users
  def welcome_email(profile, user, token)
    @profile = profile
    @user = user
    @token = token

    mail( 
      :to => @profile.email,
      :from => "Explo Admissions <portal@explo.org>",
      :reply_to => @user.get_key('program_email'),
      :subject => "Explo Portal: Login Information"
    )
  end

end
