class StudentTechForm < ActiveRecord::Base
  attr_accessor :is_yale

  belongs_to :requirement

  validate :email_details_are_correct
  validates_inclusion_of :bringing_cell_phone, :in => [true, false], :message => "Please indicate whether your child will be bringing a cell phone to campus"
  validate :phone_details_are_correct

  private

  def email_details_are_correct
    if is_yale
      errors.add(:google_email_address, "Please provide a valid email address.") unless google_email_address =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    end
  end

  def phone_details_are_correct
    if bringing_cell_phone == true
      errors.add(:mobile_phone_number, 'Please indicate your cell phone number') if mobile_phone_number.empty?
      errors.add(:mobile_phone_can_sms, 'Please indicate whether your phone has text-messaging capabilities') if mobile_phone_can_sms.nil?
      errors.add(:mobile_phone_has_data, 'Please indicate whether your phone has a data plan') if mobile_phone_has_data.nil?
    elsif bringing_cell_phone == false and is_yale
      errors.add(:will_rent_device, 'Please indicate whether you wish to rent a mobile device from Explo') if will_rent_device.nil?
    end
  end
  
end
