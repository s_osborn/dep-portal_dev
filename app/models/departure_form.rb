class DepartureForm < ActiveRecord::Base
  # Virtual attributes
  attr_accessor :is_yale, :plane_departure_time_status

  belongs_to :requirement

  validates_presence_of :departure_type, :message => 'Please choose a manner of departing Explo.'
  validate :train_details_are_correct
  validate :plane_details_are_correct


  private

  #VALIDATION LOGIC

  # If taking the train, validate details
  def train_details_are_correct
    if departure_type == 'train'
      errors.add(:train_number, 'Please indicate the train number') if train_number.empty?
      errors.add(:train_departure_time, 'Please indicate the final departure time.') if train_departure_time.empty?
      errors.add(:train_departure_time_am_pm, 'Please indicate whether the departure time is AM or PM.') if train_departure_time_am_pm.nil?
      errors.add(:train_destination_station, 'Please indicate the train destination station.') if train_destination_station.empty?
      errors.add(:train_destination_town, 'Please indicate the train destination town') if train_destination_town.empty?
    end
  end

  # If flying, make sure the details are correct
  def plane_details_are_correct
    if departure_type == 'plane'
      # ALL FLIGHTS
      errors.add(:plane_airline, 'Please indicate the airline you will be using.') if plane_airline.empty?
      errors.add(:plane_flight_number, 'Please indicate the flight number') if plane_flight_number.empty?
      errors.add(:plane_ticket_full_name, 'Please write the full name of the student, as listed on the ticket.') if plane_ticket_full_name.empty?
      
      if plane_departure_time_status == 'bad'
        errors.add(:plane_force_how_getting_to_airport, 'Please indicate how your student will be getting to the airport') if plane_force_how_getting_to_airport.nil?
        errors.add(:plane_force_how_getting_to_airport_other, 'Please indicate the OTHER method by which your student will meet their guardian at the airport') if plane_force_how_getting_to_airport == "other" && plane_force_how_getting_to_airport_other.empty?
        errors.add(:plane_force_how_getting_to_airport_comments, 'Please include contact information for the student and any people who will take the student to the airport') if plane_force_how_getting_to_airport_comments.empty?
      else
        errors.add(:plane_how_getting_to_airport, 'Please indicate how your student will be getting to the airport') if plane_how_getting_to_airport.nil?
        errors.add(:plane_how_getting_to_airport_comments, 'Please include contact information for the student and any people who will take the student to the airport') if plane_how_getting_to_airport == "meet at explo" && plane_how_getting_to_airport_comments.empty?
      end

      #UNACCOMPANIED MINOR
      errors.add(:plane_with_guardian, 'Please indicate whether the student will by flying with a parent or guardian.') if plane_with_guardian.nil?
      errors.add(:plane_um, 'Please indicate whether the student is flying as an unaccompanied minor.') if (plane_with_guardian == false && plane_um.nil?)
      if(plane_um == true && !is_yale)
        errors.add(:plane_um_fee, 'Please indicate if the unaccompanied minor fee has been paid.') if plane_um_fee.nil?
      end

    end
  end

end
