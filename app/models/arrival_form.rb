class ArrivalForm < ActiveRecord::Base
  # virtual attributes
  attr_accessor :is_yale, :plane_arrival_time_status

  belongs_to :requirement

  validates_presence_of :arrival_type, :message => 'Please choose a manner of arriving at Explo.'
  validate :car_details_are_correct
  validate :train_details_are_correct
  validate :plane_details_are_correct


  private

  #VALIDATION LOGIC

  # If arriving by car (at yale), validate details
  def car_details_are_correct
    if arrival_type == 'car'
      errors.add(:car_how, 'Please indicate how you will be arriving by car.') if car_how.nil?
      errors.add(:car_how_other, 'Please indicate how you will be arriving by car (other).') if (car_how == 'other' && car_how_other.blank?)
      errors.add(:car_service_name, 'Please indicate the name of the car service you will be using') if (car_how == 'private car' && car_service_name.blank?)
      errors.add(:car_comment, 'Please share the student\'s contact information while traveling') if car_comment.empty?
    end
  end

  # If taking the train, validate details
  def train_details_are_correct
    if arrival_type == 'train'
      errors.add(:train_number, 'Please indicate the train number') if train_number.blank?
      errors.add(:train_arrival_time, 'Please indicate the final arrival time.') if train_arrival_time.blank?
      errors.add(:train_arrival_am_pm, 'Please indicate whether the arrival time is AM or PM.') if train_arrival_am_pm.blank?
      errors.add(:train_origination_station, 'Please indicate the train origination station.') if train_origination_station.blank?
      errors.add(:train_origination_town, 'Please indicate the train origination town') if train_origination_town.blank?
      errors.add(:train_ticket_confirmation, 'Please give your train ticket confirmation number') if train_ticket_confirmation.blank?
    end
  end

  # If flying, make sure the details are correct
  def plane_details_are_correct
    if arrival_type == 'plane'
      # ALL FLIGHTS
      errors.add(:plane_flight_airline_name, 'Please select a valid airline from the drop-down airline list') if plane_flight_airline.blank?
      errors.add(:plane_flight_number, 'Please indicate the flight number') if plane_flight_number.blank?
      errors.add(:plane_ticket_full_name, 'Please write the full name of the student, as listed on the ticket.') if plane_ticket_full_name.blank?
      #errors.add(:plane_flight_destination_airport, 'Please select a valid destination airport from the drop-down airport list') if plane_flight_destination_airport.blank?
      errors.add(:plane_arrival_time, 'Please select a valid flight from the list of available flights.') if plane_arrival_time.blank?

      # UNACCOMPANIED MINOR
      errors.add(:plane_with_guardian, 'Please indicate whether the student will by flying with a parent or guardian.') if plane_with_guardian.nil?
      errors.add(:plane_is_um, 'Please indicate whether the student is flying as an unaccompanied minor.') if (plane_with_guardian == false && plane_is_um.nil?)
      errors.add(:plane_um_fee, 'Please indicate whether unaccompanied minor fees have been paid') if (!is_yale && plane_is_um == true && plane_um_fee.nil?)

      if plane_arrival_time_status == 'bad'
        # ARRIVALS WITH STRANGE TIMES
        errors.add(:plane_arrange_own_to_explo, 'Please indicate how your student will get to Explo from the airport.') if (plane_arrange_own_to_explo.nil?)
        errors.add(:plane_arrange_own_to_explo_other, 'Please indicate the OTHER method by which your student will get to Explo.') if (plane_arrange_own_to_explo == "other" && plane_arrange_own_to_explo_other.empty?)
        errors.add(:plane_arrange_own_car_service_name, 'Please indicate the name of the car service you will be using') if (plane_arrange_own_to_explo == "private car" && plane_arrange_own_car_service_name.empty?)
        errors.add(:plane_arrange_own_car_service_contact_information, 'Please indicate your student\'s contact information while traveling') if (plane_arrange_own_car_service_contact_information.blank?)
      else
        # STANDARD ARRIVALS
        errors.add(:plane_how_get_to_explo, 'Please indicate how your student will get to Explo from the airport.') if (plane_how_get_to_explo.nil? && plane_arrange_own_to_explo)
        errors.add(:plane_how_get_to_explo_comments, 'Please indicate your student\'s contact information while traveling') if (plane_how_get_to_explo == "arrange own transport" && plane_how_get_to_explo_comments.empty?)
      end
    end
  end
end
