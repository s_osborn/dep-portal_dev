class Profile < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :docusign_urls, :dependent => :destroy
  has_many :tokens
  has_secure_password

  validates :email, :presence => true, :email => true
  validates :full_name, :presence => true

  # generate a new secure token
  def add_token
    token = Token.create(token: Token.generate_token) 
    self.tokens << token
    token.token
  end
end
