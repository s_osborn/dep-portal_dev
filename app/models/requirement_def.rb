class RequirementDef < ActiveRecord::Base
  has_many :requirements, :dependent => :destroy

  has_attached_file :attachment, :styles => { :preview => ['100x100#', :png] }, :default_url => "/assets/missing.png"
  do_not_validate_attachment_file_type :attachment
end
