class Requirement < ActiveRecord::Base
  belongs_to :user
  belongs_to :requirement_def
  has_many :docusign_urls, :dependent => :destroy
  has_one :ambassador_form, :dependent => :destroy
  has_one :arrival_form, :dependent => :destroy
  has_one :course_form, :dependent => :destroy
  has_one :departure_form, :dependent => :destroy
  has_one :housing_form, :dependent => :destroy
  has_one :parent_eval_form, :dependent => :destroy
  has_one :student_tech_form, :dependent => :destroy

  def is_required
    self.requirement_def.category == 'Required' || self.requirement_def.name.match(/Required/)
  end
end
