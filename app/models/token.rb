class Token < ActiveRecord::Base
  belongs_to :profile

  def self.generate_token
    SecureRandom.hex 32
  end
end
