class Airport < ActiveRecord::Base
  def display_name
    "#{self.name} (#{self.icao})"
  end
end
