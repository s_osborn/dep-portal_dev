class AmbassadorForm < ActiveRecord::Base
  belongs_to :requirement

  validates_presence_of :interest_in_leadership, message: "Please describe your interest in leadership."
  validates_presence_of :favorite_moment, message: "Please include a favorite moment."
  validates_presence_of :three_words_self, message: "Please include three words to describe youself."
  validates_presence_of :three_words_others, message: "Please write three words other would use to describe you."
  validates_presence_of :three_words_community, message: "Please describe the Explo community in three words."
  validates_presence_of :how_contribute, message: "Please indicate how you think Explo Ambassadors can contribute to the summer."
  validates_presence_of :hope_to_gain, message: "Please indicate what you hope to gain from the Ambassador experience."
  validates_presence_of :activity_idea, message: "Please include an activity idea."
  validates_presence_of :discussion_club_idea, message: "Please include an idea for a discussion club or campus group."
  validates_presence_of :main_event_idea, message: "Please include a Main Event idea."

  validates_inclusion_of :student_agreement, in: [true, false], message: "Please indicate student agreement."
  validates_inclusion_of :parent_agreement, in: [true, false], message: "Please indicate parent agreement."

  validate :interest_in_leadership_less_than_200
  validate :favorite_moment_less_than_200
  validate :how_contribute_less_than_200
  validate :hope_to_gain_less_than_200

  def interest_in_leadership_less_than_200
    errors[:interest_in_leadership] << "Please use fewer than 200 words in your activity idea" if interest_in_leadership.split.size > 200
  end

  def favorite_moment_less_than_200
    errors[:favorite_moment] << "Please use fewer than 200 words in your activity idea" if favorite_moment.split.size > 200
  end

  def how_contribute_less_than_200
    errors[:how_contribute] << "Please use fewer than 200 words in your activity idea" if how_contribute.split.size > 200
  end

  def hope_to_gain_less_than_200
    errors[:hope_to_gain] << "Please use fewer than 200 words in your activity idea" if hope_to_gain.split.size > 200
  end
end
