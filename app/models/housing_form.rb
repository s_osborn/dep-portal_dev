class HousingForm < ActiveRecord::Base
  attr_accessor :is_residential, :is_day, :is_yale # Virtual attributes 
  belongs_to :requirement

  validates_presence_of :res_other_information,
    :if => :form_is_residential,
    :message => "Please indicate any information about your student that you'd like us to know."
  validates_presence_of :res_room_size,
    :if => :form_is_yale_residential,
    :message => "Please indicate the size of room you prefer."
  validates_presence_of :res_roommate_request_preference,
    :if => :form_is_residential,
    :message => "Please indicate roommate preference."
  validates_presence_of :res_roommate_request_name,
    :if => :res_roommate_request_activated,
    :message => "Please indicate the name of the requested roommate."

  validates_presence_of :day_other_information,
    :if => :form_is_day,
    :message => "Please indicate any information about your student that you'd like us to know."
  validates_presence_of :day_group_request_preference,
    :if => :form_is_day,
    :message => "Please indicate the group request option."
  validates_presence_of :day_group_request_name,
    :if => :day_groupmate_request_activated,
    :message => "Please indicate the name of the requested group-mate."


  # VALIDATION METHODS
  def form_is_residential
    is_residential
  end

  def form_is_yale_residential
    is_residential && is_yale
  end

  def res_roommate_request_activated
    is_residential &&
    res_roommate_request_preference != nil &&
    res_roommate_request_preference != 'no request'
  end


  def form_is_day
    is_day
  end

  def day_groupmate_request_activated
    is_day && 
    day_group_request_preference != nil && 
    day_group_request_preference != 'no request'
  end

end
