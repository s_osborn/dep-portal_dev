$ -> $('#ambassador_form_interest_in_leadership').on 'keyup', ->
  words = $('#ambassador_form_interest_in_leadership').val().match(/\S+/g).length.toString()
  $('#interest-in-leadership-count').html "#{words} words out of 200 used"

$ -> $('#ambassador_form_favorite_moment').on 'keyup', ->
  words = $('#ambassador_form_favorite_moment').val().match(/\S+/g).length.toString()
  $('#favorite-moment-count').html "#{words} words out of 200 used"

$ -> $('#ambassador_form_how_contribute').on 'keyup', ->
  words = $('#ambassador_form_how_contribute').val().match(/\S+/g).length.toString()
  $('#how-contribute-count').html "#{words} words out of 200 used"

$ -> $('#ambassador_form_hope_to_gain').on 'keyup', ->
  words = $('#ambassador_form_hope_to_gain').val().match(/\S+/g).length.toString()
  $('#hope-to-gain-count').html "#{words} words out of 200 used"
