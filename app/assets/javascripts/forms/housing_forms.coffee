$(document).on "turbolinks:load", -> 
  toggle_div(
    '#res-name-of-student-requested', 'housing_form_res_roommate_request_preference',
    ['roommate','living_group'], ['no_request'] )
  toggle_div(
    '#day-name-of-student-requested', 'housing_form_day_group_request_preference',
    ['day_group'], ['no_request'] )
