#CONDITIONAL BLOCKS
$(document).on "turbolinks:load", -> 
  # ARRIVE BY CAR
  toggle_div(
    '#workshop-selection', 'course_form_workshop_or_princeton_review',
    ['workshops', 'both'], ['princeton_review'] )
  toggle_div(
    '#princeton-review-block', 'course_form_workshop_or_princeton_review',
    ['princeton_review', 'both'], ['workshops'] )
  $('#college-admissions-read-more-link').on 'click', ->
    $('#college-admissions-read-more').slideToggle()
    return true
