$(document).on "turbolinks:load", -> 
  toggle_div(
    '#cell-phone-yes', 'student_tech_form_bringing_cell_phone',
    ['true'], ['false'] )
  toggle_div(
    '#cell-phone-no', 'student_tech_form_bringing_cell_phone',
    ['false'], ['true'] )
