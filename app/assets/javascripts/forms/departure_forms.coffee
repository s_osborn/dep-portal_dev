#CONDITIONAL BLOCKS
$(document).on "turbolinks:load", -> 
  # DEPART BY CAR
  toggle_div(
    '#depart-by-car', 'departure_form_departure_type',
    ['car'], ['plane','train'] )

  # DEPART BY TRAIN
  toggle_div(
    '#depart-by-train', 'departure_form_departure_type',
    ['train'], ['plane','car'] )

  # DEPART BY PLANE
  toggle_div(
    '#depart-by-plane', 'departure_form_departure_type',
    ['plane'], ['car','train'] )
  # UNACCOMPANIED MINOR
  toggle_div(
    '#departure-plane-unaccompanied-minor-question', 
    'departure_form_plane_with_guardian',
    ['false'], ['true'] )
  toggle_div(
    '#departure-plane-unaccompanied-minor-yes-followup-questions', 
    'departure_form_plane_um',
    ['true'], ['false'] )
  toggle_div(
    '#departure-plane-um-fee', 'departure_form_plane_um_fee',
    ['false'], ['true'] )
  # DEPARTURE METHOD: NORMAL
  toggle_div(
    '#departure-plane-meet-at-explo-arrange-own-transport', 'departure_form_plane_how_getting_to_airport',
    ['meet_at_explo'], ['accompany_to_security_continue_solo', 'shuttle'] )




# FLIGHT SEARCH + VALIDATION STUFF

flightScheduleLookupDeparture = ->
  date = "#{$('#departure_form_plane_departure_date_1i').val()}-#{$('#departure_form_plane_departure_date_2i').val()}-#{$('#departure_form_plane_departure_date_3i').val()}"
  airport = $('#departure_form_plane_non_standard_airport_code').val()
  airline = $('#departure_form_plane_airline_code').val()
  flightno = $('#departure_form_plane_flight_number').val()
  method = "departure"
  post_data = 
    date: date
    airport: airport
    airline: airline
    flightno: flightno
    method: "departure"
  if date and airport and airline and flightno
    $.ajax
      beforeSend: -> 
        $('#flight-schedule-results-pending').show()
        $('#selected-departure-looks-good').hide()
        $('#selected-departure-early').hide()
        $('#selected-departure-late').hide()
        $('#selected-departure-bad-airport').hide()
        $('#selected-departure-date-late').hide()
        $('#selected-departure-date-early').hide()
        $('#departure-plane-method-force-arrange-own-transport').hide()
        $('#departure-plane-method').show()
      complete: -> $('#flight-schedule-results-pending').hide()
      contentType: 'application/json'
      data:	JSON.stringify post_data
      dataType: 'script'
      type: 'POST'
      url: "/flights/lookup"


$(document).on "turbolinks:load", -> 
  $('#departure_form_plane_departure_date_3i').on 'change', flightScheduleLookupDeparture
  $('#departure_form_plane_non_standard_airport_name').on 'railsAutocomplete.select', flightScheduleLookupDeparture
  $('#departure_form_plane_airline').on 'railsAutocomplete.select', flightScheduleLookupDeparture
  $('#departure_form_plane_flight_number').on 'change', flightScheduleLookupDeparture
  $('#flight-search-button').on 'click', flightScheduleLookupDeparture




# Validate selected flight and show some warnings if it is outside of our
# default arrival times
checkSelectedFlightDeparture = (departure_hour, am_pm) ->
  departure_date = "#{$('#departure_form_plane_departure_date_1i').val()}-#{$('#departure_form_plane_departure_date_2i').val()}-#{$('#departure_form_plane_departure_date_3i').val()}"
  user_departure_date = "#{$('#departure_form_user_departure_date').val()}"
  airport_code = $('#departure_form_plane_non_standard_airport_code').val()
  default_airport_code = $('#departure_form_default_airport_code').val()

  if (((departure_hour >= 8 and departure_hour <= 11 and am_pm == "AM") or ((departure_hour == '12' or departure_hour == '1') and am_pm == "PM")) and (departure_date == user_departure_date) and (airport_code == default_airport_code))
    $('#departure-plane-method-force-arrange-own-transport').hide()
    $('#departure-plane-method').show()
    $('#selected-departure-looks-good').show()
    $('#selected-departure-early').hide()
    $('#selected-departure-late').hide()
    $('#selected-departure-bad-airport').hide()
    $('#selected-departure-date-late').hide()
    $('#selected-departure-date-early').hide()
    $('#departure_form_plane_departure_time_status').val('good')
  else if (departure_hour < 8 and am_pm == "AM" and (departure_date == user_departure_date) and (airport_code == default_airport_code))
    $('#departure-plane-method-force-arrange-own-transport').show()
    $('#departure-plane-method').hide()
    $('#selected-departure-looks-good').hide()
    $('#selected-departure-early').show()
    $('#selected-departure-late').hide()
    $('#selected-departure-bad-airport').hide()
    $('#selected-departure-date-late').hide()
    $('#selected-departure-date-early').hide()
    $('#departure_form_plane_departure_time_status').val('bad')
  else if (departure_hour > 1 and am_pm == "PM" and (departure_date == user_departure_date) and (airport_code == default_airport_code))
    $('#departure-plane-method-force-arrange-own-transport').hide()
    $('#departure-plane-method').show()
    $('#selected-departure-looks-good').hide()
    $('#selected-departure-early').hide()
    $('#selected-departure-late').show()
    $('#selected-departure-bad-airport').hide()
    $('#selected-departure-date-late').hide()
    $('#selected-departure-date-early').hide()
    $('#departure_form_plane_departure_time_status').val('bad')
  else if (departure_date > user_departure_date)
    $('#departure-plane-method-force-arrange-own-transport').show()
    $('#departure-plane-method').hide()
    $('#selected-departure-looks-good').hide()
    $('#selected-departure-early').hide()
    $('#selected-departure-late').hide()
    $('#selected-departure-bad-airport').hide()
    $('#selected-departure-date-late').show()
    $('#selected-departure-date-early').hide()
    $('#departure_form_plane_departure_time_status').val('bad')
  else if (departure_date < user_departure_date)
    $('#departure-plane-method-force-arrange-own-transport').show()
    $('#departure-plane-method').hide()
    $('#selected-departure-looks-good').hide()
    $('#selected-departure-early').hide()
    $('#selected-departure-bad-airport').hide()
    $('#selected-departure-date-late').hide()
    $('#selected-departure-date-early').show()
    $('#departure_form_plane_departure_time_status').val('bad')
  else if (airport_code != default_airport_code)
    $('#departure-plane-method-force-arrange-own-transport').show()
    $('#departure-plane-method').hide()
    $('#selected-departure-looks-good').hide()
    $('#selected-departure-early').hide()
    $('#selected-departure-barely-allowed').hide()
    $('#selected-departure-bad-airport').show()
    $('#selected-departure-date-late').hide()
    $('#selected-departure-date-early').hide()
    $('#departure_form_plane_departure_time_status').val('bad')
  $('#departure-plane-method-force-arrange-own-transport').show() if airport_code != default_airport_code

# When you get a list of results back from the FlightAware search, you'll want
# to select one. This is that.
selectFlightDeparture = ->
  $('.select-this-flight').on 'click', ->
    $(this).parent().parent().children().removeClass('selected')
    $(this).parent().addClass('selected')

    td = $(this).siblings('[data-arrival-hour]')

    am_pm = td.attr('data-am-pm')
    departure_hour = td.attr('data-arrival-hour')
    departure_time = td.attr('data-arrival-time')

    $('#departure_form_plane_departure_time').val(departure_time)
    $('#departure_form_plane_departure_time_am_pm').val(am_pm)

    checkSelectedFlightDeparture(departure_hour, am_pm)

# Make selecting code global so we can (re)run it when the AJAX call is made
window.selectFlightDeparture = selectFlightDeparture 

# When the departure form loads, run the check for any departure time values
# that might have been previously selected
$(document).on "turbolinks:load", -> 
  if $('#departure_form_plane_departure_time').val()
    checkSelectedFlightDeparture($('#departure_form_plane_departure_time').val().match(/^../)[0], $('#departure_form_plane_departure_time_am_pm').val())
