# given a field, return a function that will submit the stay late form,
# applying the check box value to the given field in the form
submitStayLate = (field) ->
  () ->
    form_id = $(this).data('form-id')
    form_value = $(this).data('form-value')
    if form_value == undefined or form_value == false
      form_value = true
      display_value = '<input type="checkbox" checked>'
    else
      form_value = false
      display_value = '<input type="checkbox">'

    post_data = {}
    post_data['id'] = form_id
    post_data[field] = form_value

    url = "/stay_late_forms/#{form_id}"

    unless $(this).children().first().attr('disabled')
      $.ajax
        method: 'PUT'
        complete: (element = this) ->
          $(".overnight[data-form-id=#{form_id}]").html(display_value).fadeIn(3000)
          $(".overnight[data-form-id=#{form_id}]").data("form-value", form_value)
        contentType: 'application/json'
        data:	JSON.stringify post_data
        dataType: 'script'
        type: 'POST'
        url: url

$ -> $('.overnight').on 'click', submitStayLate('overnight')
$ -> $('.stay-late').on 'click', submitStayLate('stay_late')
