#CONDITIONAL BLOCKS
$(document).on "turbolinks:load", -> 
  # ARRIVE BY CAR
  toggle_div(
    '#arrive-by-car', 'arrival_form_arrival_type',
    ['car'], ['plane','train'] )
  # USING CAR SERVICE
  toggle_div(
    '#arrival-car-service-name', 'arrival_form_car_how',
    ['private_car'], ['family_member','other'] )

  # ARRIVE BY TRAIN
  toggle_div(
    '#arrive-by-train', 'arrival_form_arrival_type',
    ['train'], ['car','plane'] )

  # ARRIVE BY PLANE
  toggle_div(
    '#arrive-by-plane', 'arrival_form_arrival_type',
    ['plane'], ['car','train'] )
  # UNACCOMPANIED MINOR
  toggle_div(
    '#arrival-plane-unaccompanied-minor-question',
    'arrival_form_plane_with_guardian',
    ['false'], ['true'] )
  toggle_div(
    '#arrival-plane-um-fee-question', 'arrival_form_plane_is_um',
    ['true'], ['false'] )
  toggle_div(
    '#arrival-plane-um-fee-notice', 'arrival_form_plane_um_fee',
    ['false'], ['true'] )
  # ARRIVAL METHOD
  toggle_div(
    '#arrival-plane-standard-shuttle-family',
    'arrival_form_plane_how_get_to_explo',
    ['shuttle_family'], ['arrange_own_transport', 'shuttle_student'] )
  toggle_div(
    '#arrival-plane-standard-arrange-own-transport-comments',
    'arrival_form_plane_how_get_to_explo',
    ['arrange_own_transport'], ['shuttle_family', 'shuttle_student'] )
  # ARRANGE OWN PRIVATE CAR
  toggle_div(
    '#arrival-plane-force-arrange-own-car-service-name',
    'arrival_form_plane_arrange_own_to_explo',
    ['private_car'], ['other', 'family_member'] )




# FLIGHT VALIDATION STUFF
flightScheduleLookupArrival = ->
  date = "#{$('#arrival_form_plane_arrival_date_1i').val()}-#{$('#arrival_form_plane_arrival_date_2i').val()}-#{$('#arrival_form_plane_arrival_date_3i').val()}"
  airport = $('#arrival_form_plane_flight_destination_airport').val()
  airline = $('#arrival_form_plane_flight_airline').val()
  flightno = $('#arrival_form_plane_flight_number').val()
  post_data = 
    date: date
    airport: airport
    airline: airline
    flightno: flightno
    method: "arrival"
  if date and airport and airline and flightno
    $.ajax
      beforeSend: -> 
        $('#flight-schedule-results-pending').show()
        $('#selected-arrival-looks-good').hide()
        $('#selected-arrival-barely-allowed').hide()
        $('#selected-arrival-outside-allowed-times').hide()
        $('#arrival-plane-force-arrange-own-transport').hide()
        $('#arrival-plane-standard-method').show()
      complete: -> $('#flight-schedule-results-pending').hide()
      contentType: 'application/json'
      data:	JSON.stringify post_data
      dataType: 'script'
      type: 'POST'
      # send search to the Portal flight lookup controller
      url: "/flights/lookup"

# Set up auto-update of flight lookup on field change
$(document).on "turbolinks:load", -> 
  $('#arrival_form_arrival_date_3i').on 'change', flightScheduleLookupArrival
  $('#arrival_form_plane_flight_destination_airport_name').on 'railsAutocomplete.select', flightScheduleLookupArrival
  $('#arrival_form_plane_flight_airline_name').on 'railsAutocomplete.select', flightScheduleLookupArrival
  $('#arrival_form_plane_flight_number').on 'change', flightScheduleLookupArrival
  $('#arrival-flight-search-button').on 'click', flightScheduleLookupArrival


# Validate selected flight and show some warnings if it is outside of our
# default arrival times
checkSelectedFlightArrival = (arrival_hour, am_pm) ->
  arrival_date = "#{$('#arrival_form_plane_arrival_date_1i').val()}-#{$('#arrival_form_plane_arrival_date_2i').val()}-#{$('#arrival_form_plane_arrival_date_3i').val()}"
  user_arrival_date = "#{$('#arrival_form_user_arrival_date').val()}"
  airport = $('#arrival_form_plane_flight_destination_airport').val()
  default_airport = $('#arrival_form_default_destination_airport').val()

  if ((arrival_hour < 5 or arrival_hour == '12') and am_pm == "PM" and (arrival_date == user_arrival_date) and (airport == default_airport))
    $('#selected-arrival-looks-good').show()
    $('#selected-arrival-barely-allowed').hide()
    $('#selected-arrival-outside-allowed-times').hide()
    $('#arrival-plane-force-arrange-own-transport').slideUp()
    $('#arrival-plane-standard-method').slideDown()
    $('#arrival_form_plane_arrival_time_status').val('good')
  else if (((arrival_hour > 9 and am_pm == "AM") or (arrival_hour < 8 and am_pm == "PM")) and (arrival_date == user_arrival_date) and (airport == default_airport))
    $('#selected-arrival-looks-good').hide()
    $('#selected-arrival-barely-allowed').show()
    $('#selected-arrival-outside-allowed-times').hide()
    $('#arrival-plane-force-arrange-own-transport').slideUp()
    $('#arrival-plane-standard-method').slideDown()
    $('#arrival_form_plane_arrival_time_status').val('ok')
  else
    $('#selected-arrival-looks-good').hide()
    $('#selected-arrival-barely-allowed').hide()
    $('#selected-arrival-outside-allowed-times').show()
    $('#arrival-plane-force-arrange-own-transport').slideDown()
    $('#arrival-plane-standard-method').slideUp()
    $('#arrival_form_plane_arrival_time_status').val('bad')

# When the arrival form loads, run the check for any arrival time values that might
# have been previously selected
$(document).on "turbolinks:load", ->
  if $('#arrival_form_plane_arrival_time').val()
    checkSelectedFlightArrival($('#arrival_form_plane_arrival_time').val().match(/^../)[0], $('#arrival_form_plane_arrival_time_am_pm').val())

# When you get a list of results back from the FlightAware search, you'll want
# to select one. This is that. It's actually called from the
# FlightSearchController, but we define it here to keep things consolidated.
selectFlightArrival = ->
  $('.select-this-flight').on 'click', ->
    $(this).parent().parent().children().removeClass('x-bg-paleorange')
    $(this).parent().addClass('x-bg-paleorange')

    td = $(this).siblings('[data-arrival-hour]')

    am_pm = td.attr('data-am-pm')
    arrival_hour = td.attr('data-arrival-hour')
    arrival_time = td.attr('data-arrival-time')

    $('#arrival_form_plane_arrival_time').val(arrival_time)
    $('#arrival_form_plane_arrival_time_am_pm').val(am_pm)

    checkSelectedFlightArrival(arrival_hour, am_pm)
# Make selecting code global so we can (re)run it when the AJAX call is made
# from the FlightSearchController
window.selectFlightArrival = selectFlightArrival 
