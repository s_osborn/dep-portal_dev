$(document).on "turbolinks:load", () -> 
  campus_name = $('.atlas-pdf-links').data('campus-name')
  grade = $('.atlas-pdf-links').data('grade')
  grade_level = switch grade
    when 2,3,4,5 then " - Pioneer + Explorer"
    when 6,7 then " - Voyager"
    else ""
  url = "https://explo-summer-atlas.herokuapp.com/pdf/#{campus_name}#{grade_level}"
  if campus_name && grade
    $.ajax 
      url: url
      dataType: 'jsonp'
      success: (result) ->
        $('.atlas-pdf-links').html('(')
        result = $.parseJSON result 
        for link, i in result
          $('.atlas-pdf-links').append("<a href=\"#{link.attrs.AtlasPDF[0].url}\" target=\"atlas\" class=\"link x-orange x-hover-darkorange pointer\">#{link.attrs.Date}</a>")
          $('.atlas-pdf-links').append(' | ') unless i == result.length - 1
        $('.atlas-pdf-links').append(')')
        $('.atlas-blurb').show() if result[0]
