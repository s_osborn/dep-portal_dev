# toggle_div is a global utility function for radio-button fields that trigger
#  divs showing/hiding.
# given a div, a "field" which is really the rails-rendered field ID (minus
#  any value), and a list of "show values" and "hide values", this function
#  will take care of making sure that the div is shown or hidden when the
#  appropriate values are set on that field, including on page load/reload and
#  when viewing an already-submitted form.
window.toggle_div = (div, field, show_values, hide_values) ->
  # (assumes the div starts out hidden, and shows when fields are checked)
  # make sure the div is hidden on page load if the field is already checked
  for value in hide_values
    choice_id = "##{field}_#{value}"
    $(div).hide() if $(choice_id).attr('checked')
    $(choice_id).on 'click', -> $(div).slideUp()
  for value in show_values
    choice_id = "##{field}_#{value}"
    $(div).show() if $(choice_id).attr('checked')
    $(choice_id).on 'click', -> $(div).slideDown()


$(document).on "turbolinks:load", -> 
  # make sure that fields with errors are wrapped in the appropriate tachyons
  #  classes
  $('.field_with_errors').children().addClass('ba bw1 x-b--red b--dotted')
  $('.field_with_errors').addClass('di')

  # Colpitts blurb on Summer Forms Index
  $('#colpitts-show-more-information').on 'click', ->
    $('#colpitts-more-information').slideToggle()
