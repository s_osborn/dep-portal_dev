module ApplicationHelper
  include Concerns::SessionUtils
  include ExploCssHelper

  def is_admin
    session_is_admin || session_profile.try(:is_admin)
  end

  def user_info(key)
    @current_user.get_key(key)
  end

  def user_match(key, value)
    @current_user.match(key,value)
  end
end
