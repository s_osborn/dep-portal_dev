class Forms::StudentTechFormsController < ApplicationController
  include Concerns::Forms::Layout

  # GET /student_tech_forms/1/edit
  def edit
    do_edit "student_tech"
  end

  # PUT /student_tech_forms/1
  # PUT /student_tech_forms/1.json
  def update
    @student_tech_form = StudentTechForm.find(params[:id])
    check_form_allowed(@student_tech_form, @current_user)

    respond_to do |format|
      if @student_tech_form.update(student_tech_form_params)
        @student_tech_form.requirement.is_complete = true
        @student_tech_form.requirement.save

        if HiveService.post_atom("student_tech", "update", @student_tech_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@student_tech_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Student tech form was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @student_tech_form.errors, status: :unprocessable_entity }
      end
    end
  end



private

  # Never trust parameters from the scary internet, only allow the white list through.
  def student_tech_form_params
    params.require(:student_tech_form).permit(:is_yale, :google_email_address, :mobile_phone_number, :bringing_cell_phone, :mobile_phone_can_sms, :mobile_phone_has_data, :will_rent_device)
  end

end
