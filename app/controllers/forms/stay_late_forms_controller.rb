class Forms::StayLateFormsController < ApplicationController
  include Concerns::Common::DateVariables

  before_action :check_api_auth, only: [:fm]
  skip_before_action :check_user_login, only: [:update]
  skip_before_action :verify_authenticity_token, only: [:update]

  layout 'dashboard'

  # GET /stay_late_forms
  # Note that this is a user-facing index, which only shows the forms related
  # to the logged-in user
  def index
    #create empty forms if there are none
    if @current_user.stay_late_forms.empty?
      @current_user.try(:session_arrival_date).try(:upto, @current_user.try(:session_departure_date)).try(:each) do |day|
        @current_user.stay_late_forms.create(day: day)
      end
    end
  end

  # GET /stay_late_forms/fm
  # FileMaker-specific view of stay-late forms. Requires API authentication
  # Expects a Portal `user_id` parameter to be passed, or it won't work
  def fm
    @user = User.find_by(id: params[:user_id])
    @stay_late_forms = @user.try(:stay_late_forms)
    render layout: 'filemaker'
  end

  # PUT /stay_late_forms/1.json
  # called via AJAX
  def update
    @stay_late_form = StayLateForm.find(params[:id])

    respond_to do |format|
      if @stay_late_form.update_attributes(stay_late_form_params)
        format.json { head :no_content }
      else
        format.json { render json: @stay_late_form.errors, status: :unprocessable_entity }
      end
    end
  end


  private

  def stay_late_form_params
    params.require(:stay_late_form).permit(:overnight, :stay_late)
  end
end
