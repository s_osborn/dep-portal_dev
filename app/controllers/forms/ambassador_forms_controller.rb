class Forms::AmbassadorFormsController < ApplicationController
  include Concerns::Forms::Layout

  # GET /ambassador_forms/1/edit
  def edit
    do_edit "ambassador"
  end

  # PUT /ambassador_forms/1
  # PUT /ambassador_forms/1.json
  def update
    @ambassador_form = AmbassadorForm.find(params[:id])
    check_form_allowed(@ambassador_form, @current_user)

    respond_to do |format|
      if @ambassador_form.update_attributes(ambassador_form_params)
        @ambassador_form.requirement.is_complete = true
        @ambassador_form.requirement.save

        if HiveService.post_atom("ambassador", "update", @ambassador_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@ambassador_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Ambassador form was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ambassador_form.errors, status: :unprocessable_entity }
      end
    end
  end


private

  # Never trust parameters from the scary internet, only allow the white list through.
  def ambassador_form_params
    params.require(:ambassador_form).permit(:interest_in_leadership, :favorite_moment, :three_words_self, :three_words_others, :three_words_community, :how_contribute, :hope_to_gain, :activity_idea, :discussion_club_idea, :main_event_idea, :student_agreement, :parent_agreement)
  end
end
