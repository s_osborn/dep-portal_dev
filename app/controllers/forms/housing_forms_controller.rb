class Forms::HousingFormsController < ApplicationController
  include Concerns::Forms::Layout

  def edit
    do_edit "housing"
  end

  def update
    @housing_form = HousingForm.find_by_id(params[:id]) || HousingForm.new
    check_form_allowed(@housing_form, @current_user)
    respond_to do |format|
      if @housing_form.update_attributes(housing_form_params)
        @housing_form.requirement.is_complete = true
        @housing_form.requirement.save

        if HiveService.post_atom("housing", "update", @housing_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@housing_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Form successfully saved.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  private

  def housing_form_params
    params.require(:housing_form).permit(:is_residential, :is_day, :is_yale, :day_group_request_name, :day_group_request_preference, :day_other_information, :res_other_information, :res_room_size, :res_roommate_request_name, :res_roommate_request_preference)
  end
end
