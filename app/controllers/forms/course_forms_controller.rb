class Forms::CourseFormsController < ApplicationController
  include Concerns::Forms::Layout

  def edit
    do_edit "course"
    get_select_choices
  end

  def update
    @course_form = CourseForm.find_by_id(params[:id]) || CourseForm.new
    check_form_allowed(@course_form, @current_user)
    respond_to do |format|
      if @course_form.update_attributes(course_form_params)
        @course_form.requirement.is_complete = true
        @course_form.requirement.save

        if HiveService.post_atom("course", "update", @course_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@course_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Form successfully saved.' }
      else
        format.html {
          get_select_choices
          render action: "edit"
        }
      end
    end
  end

  private

  # get selection choices for fields in the form.
  def get_select_choices
    # Set period numbers
    ll = @current_user.get_key('language_level') || 4
    if(@current_user.match('s1_campus', 'Yale University') || @current_user.match('s2_campus', 'Yale University'))
      # YALE
      @period1 = 1
      @period2 = 2
      @period3 = "W3"
      @period4 = "W4"
      @program_code = "SEN"
      if(@current_user.match('language_level','2') || @current_user.match('language_level','3'))
        @has_esol = 1
        @period_count = 5
        @p1_c1_esol = [['114: ESOL ($500)','114']]
        @p2_c1_esol = [['214: ESOL ($500)','214']]
      else
        @has_esol = 0
        @period_count = 6
      end
      @period1_courses = Course.where('program_code=? AND course_type="Course" AND year=? AND language_level <= ? AND (code LIKE ? OR code LIKE ?)', @program_code, @current_year, ll, "#{@period1}%", "%¶#{@period1}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period2_courses = Course.where('program_code=? AND course_type="Course" AND year=? AND language_level <= ? AND (code LIKE ? OR code LIKE ?)', @program_code, @current_year, ll, "#{@period2}%", "%¶#{@period2}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period3_workshops = Course.where('program_code=? AND course_type="Workshop" AND year=? AND language_level <= ? AND code LIKE ?', @program_code, @current_year, ll, "%#{@period3}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period4_workshops = Course.where('program_code=? AND course_type="Workshop" AND year=? AND language_level <= ? AND code LIKE ?', @program_code, @current_year, ll, "%#{@period4}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }

    elsif (@current_user.match('s1_campus', 'Wellesley College') || @current_user.match('s2_campus', 'Wellesley College'))
      # WELLESLEY
      @period1 = 3
      @period2 = 4
      @program_code = "INT"
      @workshop_code = "W5"
      @period1_courses = Course.where('program_code=? AND course_type="Course" AND year=? AND language_level <= ? AND (code LIKE ? OR code LIKE ?)', @program_code, @current_year, ll, "#{@period1}%", "%¶#{@period1}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period2_courses = Course.where('program_code=? AND course_type="Course" AND year=? AND language_level <= ? AND (code LIKE ? OR code LIKE ?)', @program_code, @current_year, ll, "#{@period2}%", "%¶#{@period2}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @workshops = Course.where('program_code=? AND course_type="Workshop" AND year=? AND language_level <= ? AND code LIKE ?', @program_code, @current_year, ll, "%#{@workshop_code}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }

    elsif (@current_user.match('s1_campus', 'Wheaton College') || @current_user.match('s2_campus', 'Wheaton College') || @current_user.get_key('w1_program_name') || @current_user.get_key('w2_program_name') || @current_user.get_key('w3_program_name') || @current_user.get_key('w4_program_name') || @current_user.get_key('w5_program_name') || @current_user.get_key('w6_program_name'))
      # WHEATON CAMPUS
      if (@current_user.get_key('s1_program_name') || @current_user.get_key('s2_program_name'))
        @program_code = "JUN"
      elsif (@current_user.get_key('w1_program_name') || @current_user.get_key('w2_program_name') || @current_user.get_key('w3_program_name') || @current_user.get_key('w4_program_name') || @current_user.get_key('w5_program_name') || @current_user.get_key('w6_program_name'))
        @program_code = "MAW"
      end
      @workshop_code = "W1"
      @period1 = "W1"
      @period2 = "W2"
      @period3 = "W3"
      if @current_user.get_key('grade').to_i > 5
        #VOYAGERS
        @p1_c1_esol = '730'

      else
        # PIONEERS
        @p1_c1_esol = '619'
      end

      @period1_courses = Course.where('program_code LIKE ? AND course_type="Course" AND year=? AND language_level <= ? AND period LIKE ?', "%#{@program_code}%", @current_year, ll, "%#{@period1}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period2_courses = Course.where('program_code LIKE ? AND course_type="Course" AND year=? AND language_level <= ? AND period LIKE ?', "%#{@program_code}%", @current_year, ll, "%#{@period2}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @period3_courses = Course.where('program_code LIKE ? AND course_type="Course" AND year=? AND language_level <= ? AND period LIKE ?', "%#{@program_code}%", @current_year, ll, "%#{@period3}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
      @workshops = Course.where('program_code LIKE ? AND course_type="Workshop" AND year=? AND language_level <= ? AND code LIKE ?', "%#{@program_code}%", @current_year, ll, "%#{@workshop_code}%").order(:code).map { |c| ["#{c.code.gsub("¶",",")}: #{c.fun_title}", c.code] }
    end

  end


private

  # Never trust parameters from the scary internet, only allow the white list through.
  def course_form_params
    params.require(:course_form).permit(:campus, :is_fluent, :is_session_one, :is_session_two, :p1_c1, :p1_c2, :p1_c3, :p1_c4, :p1_c5, :p1_c6, :p2_c1, :p2_c2, :p2_c3, :p2_c4, :p2_c5, :p2_c6, :p3_c1, :p3_c2, :p3_c3, :p3_c4, :p3_c5, :p3_c6, :p4_c1, :p4_c2, :p4_c3, :p4_c4, :p4_c5, :p4_c6, :w_c1, :w_c2, :w_c3, :w_c4, :w_c5, :w_c6, :workshop_or_princeton_review, :wants_crew, :is_session_one, :session_1_weekend_1, :session_1_weekend_2, :session_2_weekend_1, :session_2_weekend_2)
  end
end
