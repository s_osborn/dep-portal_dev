class Forms::ArrivalFormsController < ApplicationController
  include Concerns::Forms::Layout

  def edit
    do_edit "arrival"
    set_airport_and_icao
  end

  def update
    @arrival_form = ArrivalForm.find_by_id(params[:id]) || ArrivalForm.new
    check_form_allowed(@arrival_form, @current_user)
    set_airport_and_icao
    
    respond_to do |format|
      if @arrival_form.update_attributes(arrival_form_params)
        @arrival_form.requirement.is_complete = true
        @arrival_form.requirement.save

        if HiveService.post_atom("arrival", "update", @arrival_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@arrival_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Form successfully saved.' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  private

  def set_airport_and_icao
    if (@current_user.match('s1_campus', 'Yale University') || (@current_user.match('s2_campus', 'Yale University') && !@current_user.match('s1_campus','Wellesley College')))
      @airport_name = 'John F Kennedy Intl (KJFK)'
      @arrival_form.plane_flight_destination_airport = 'KJFK'
      @airport_name = 'JFK Airport'
      @airport_code = 'KJFK'
    else 
      @airport_name = 'Boston Logan Intl (KBOS)'
      @arrival_form.plane_flight_destination_airport = 'KBOS'
      @airport_name = 'Logan Airport'
      @airport_code = 'KBOS'
    end
    if @arrival_form.plane_flight_destination_airport
      @airport_code = @arrival_form.plane_flight_destination_airport
    end
  end


private

  # Never trust parameters from the scary internet, only allow the white list through.
  def arrival_form_params
    params.require(:arrival_form).permit(:is_yale, :plane_arrival_time_status, :arrival_type, :car_comment, :car_how, :car_how_other, :car_service_name, :plane_arrange_own_car_service_contact_information, :plane_arrange_own_car_service_name, :plane_arrange_own_to_explo, :plane_arrange_own_to_explo_other, :plane_arrival_date, :plane_arrival_time, :plane_arrival_time_am_pm, :plane_flight_airline, :plane_flight_airline_name, :plane_flight_destination_airport, :plane_flight_number, :plane_how_get_to_explo, :plane_how_get_to_explo_comments, :plane_is_um, :plane_ticket_full_name, :plane_um_fee, :plane_with_guardian, :train_arrival_am_pm, :train_arrival_time, :train_number, :train_origination_station, :train_origination_town, :train_ticket_confirmation)
  end
  
end
