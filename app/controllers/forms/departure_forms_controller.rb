class Forms::DepartureFormsController < ApplicationController
  include Concerns::Forms::Layout

  def edit
    do_edit "departure"
    get_airport_names_and_codes

  end

  def update
    @departure_form = DepartureForm.find_by_id(params[:id]) || DepartureForm.new
    check_form_allowed(@departure_form, @current_user)

    get_airport_names_and_codes

    respond_to do |format|
      if @departure_form.update_attributes(departure_form_params)
        @departure_form.requirement.is_complete = true
        @departure_form.requirement.save

        if HiveService.post_atom("departure", "update", @departure_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@departure_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Form successfully saved.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end


  private

  def get_airport_names_and_codes
    if (@current_user.match('s1_campus', 'Yale University') || @current_user.match('s2_campus', 'Yale University')) 
      @airport_name = 'JFK Airport'
      @airport_code = 'KJFK'
    else
      @airport_name = 'Logan Airport'
      @airport_code = 'KBOS'
    end
    @departure_form.plane_non_standard_airport_code = @airport_code unless @departure_form.plane_non_standard_airport_code
    @departure_form.plane_non_standard_airport_name = @airport_name unless @departure_form.plane_non_standard_airport_name 
  end


private

  # Never trust parameters from the scary internet, only allow the white list through.
  def departure_form_params
    params.require(:departure_form).permit(:is_yale, :plane_departure_time_status, :departure_type, :plane_airline, :plane_airline_code, :plane_departure_date, :plane_departure_time, :plane_departure_time_am_pm, :plane_destination_airport, :plane_flight_number, :plane_force_how_getting_to_airport, :plane_force_how_getting_to_airport_comments, :plane_force_how_getting_to_airport_other, :plane_how_getting_to_airport, :plane_how_getting_to_airport_comments, :plane_non_standard_airport_code, :plane_non_standard_airport_name, :plane_non_standard_airport_other, :plane_um, :plane_um_fee, :plane_um_name, :plane_with_guardian, :plane_ticket_full_name, :train_departure_time, :train_departure_time_am_pm, :train_destination_station, :train_destination_town, :train_number)
  end
end
