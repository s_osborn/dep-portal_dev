class Forms::ParentEvalFormsController < ApplicationController
  include Concerns::Forms::Layout

  def edit
    do_edit "parent_eval"
  end

  def update
    @parent_eval_form = ParentEvalForm.find_by_id(params[:id]) || ParentEvalForm.new
    check_form_allowed(@parent_eval_form, @current_user)
    respond_to do |format|
      if @parent_eval_form.update_attributes(parent_eval_form_params)
        @parent_eval_form.requirement.is_complete = true
        @parent_eval_form.requirement.save

        if HiveService.post_atom("parent_eval", "update", @parent_eval_form).nil?
          logger.error "ERROR: HIVE ATOM POST: #{@parent_eval_form.attributes}"
        end

        format.html { redirect_to forms_home_url, notice: 'Form successfully saved.' }
      else
        format.html { render action: "edit" }
      end
    end
  end


private

  # Never trust parameters from the scary internet, only allow the white list through.
  def parent_eval_form_params
    params.require(:parent_eval_form).permit(:form_type, :adjusting_homesickness, :attend_before, :concerns, :describe_socially, :ethnic_affiliation, :ethnicity_drop_down, :expectations, :interests, :general_information, :learning_style, :takes_medications)
  end

end
