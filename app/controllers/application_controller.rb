class ApplicationController < ActionController::Base
  # API Authentication is where we store methods that are necessary to validate
  # API calles (as distinct from "regular" web users
  include Concerns::ApiAuthentication
  # RedirectUtils is where we store utility functions for redirects, as well as
  # utility functions to check validity of various app contructs + redirect
  # based on that validity
  include Concerns::RedirectUtils
  # SessionUtils provides us with methods to encapsulate session access.
  include Concerns::SessionUtils


  before_action :check_user_login
  force_ssl if: :ssl_configured?
  protect_from_forgery :except => :api_auth_check


  private
    
  def ssl_configured?
    !Rails.env.development?
  end
end
