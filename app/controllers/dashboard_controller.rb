class DashboardController < ApplicationController
  include Concerns::Dashboard::Layout

  before_action :set_form_lists

  def index
    #@summer_resources_debug = APP_CONFIG['debug']
    @summer_resources_debug = false
  end


  private 

  # lists of forms are required in the dashboard view
  def set_form_lists
    all_reqs = session_user.try(:requirements).try(:sort_by) { |s| "#{s.requirement_def.sort_order}#{s.requirement_def.name}" }
    @required_forms = []
    @medical_forms = []
    @optional_forms = []
    @trip_waivers = []

    all_reqs.try(:each) do |req|
      @required_forms << req if req.is_required
      @medical_forms << req if req.requirement_def.category == "Medical"
      @optional_forms << req if req.requirement_def.category == "Optional"
      @trip_waivers << req if (req.requirement_def.category == "Waiver" && !req.is_required)
    end

    @medical_forms_complete = true if @medical_forms.first && @medical_forms.first.is_complete
  end
end
