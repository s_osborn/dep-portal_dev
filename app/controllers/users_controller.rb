class UsersController < ApplicationController
  # Adding new users is API-only at this time
  before_action :check_api_auth
  skip_before_action :check_user_login
  skip_before_action :verify_authenticity_token

  layout 'basic'

  # Update an existing user
  def batch_update
    @users = []
    users = JSON.parse params[:users]
    users.each do |user|
      portal_id = user['portal_user_ID']
      email = user['email']
      full_name = user['full_name']
      is_admin = user['is_admin']
      password = user['password'] || rand(2**64)
      portico_id = user['contact_ID']
      campdoc_profile_id = user['campdoc_profile_id']
      
      # check if portal user already exists
      portal_user = User.find_by_portico_id portico_id
      portal_profile = Profile.find_by_email email
      profile_is_new = false
      user_is_new = false

      # create a new profile if none was found associated with the user email
      unless portal_profile
        portal_profile = Profile.create!(
          email: email, 
          full_name: full_name,
          password: password,
          is_admin: is_admin
        )
        profile_is_new = true
      end

      # create a new user if none was found
      unless portal_user
        # add a random password unless one was sent
        portal_user = User.create!({
          portico_id: portico_id,
          campdoc_profile_id: campdoc_profile_id
        })
        logger.info("USER ERRORS: #{portal_user.errors}") if portal_user.errors
        
        # associate the email profile with this user (if it hasn't already happened)
        portal_profile.users << portal_user
        
        user_is_new = true
      end

      # update campdoc profile information if we haven't already (shouldn't be
      # necessary since the call to associate campdoc profiles should do this.
      # This is a belt + suspenders way of making sure we've got it)
      if campdoc_profile_id != nil && portal_user.campdoc_profile_id == nil
        portal_user.campdoc_profile_id = campdoc_profile_id
        portal_user.save
      end

      if user['keys']
        # making the big fat assumption that we're getting a full list of keys
        # thus we clear out the existing information before adding any keys
        portal_user.information = nil
        user['keys'].each do |key, value|
          portal_user.set_key(key, value)
        end
      end

      if user['requirements']
        user['requirements'].each do |req|
          #logger.info "updating requirement for #{portal_user.username}:#{req['code']}, #{req['status']}"
          portal_user.add_requirement(req['code'], req['status'], req['mod_on'])
        end
      end

      # generate a security token for auto-login via email if this is a new profile
      if profile_is_new
        Notifier.welcome_email(portal_profile, portal_user, portal_profile.add_token).deliver
      elsif user_is_new
        #if it's a new user, just notify the family that they've been added
        Notifier.add_profile(portal_profile, portal_user, portal_profile.full_name).deliver
      end


      @users << portal_user
    end

    respond_to do |format|
      format.html { render :json => @users }
      format.json { render :json => @users }
    end
  end

  # Delete a user (by ID)
  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      render :json => "{'success':'true'}"
    else
      render :json => "{'success':'false'}"
    end
  end

  # Update an existing user
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])

        if params[:keys]
          params[:keys].each do |key, value|
            @user.set_key(key, value)
          end
        end

        format.html { redirect_to root_url, notice: 'User information successfully updated' }
        format.json { render :json => @user }
      else
        format.html { 
          errors = @user.errors.messages.collect { |key,val| "#{key} #{val[0]}" } 
          redirect_to back, alert: "Error saving user information: #{errors}" 
        }
        format.json { render :json => '"validation_error":"true"' }
      end

    end
  end

end
