class Campdoc::ProfilesController < ApplicationController
  before_action :check_api_auth 

  # POST /campdoc/profiles
  # expects parameters:
  # contact_id : portico contact ID
  # profile_json : profile information as per
  #   https://github.com/docnetwork/api/blob/master/chapters/02-profiles.md
  def create
    contact_id = params[:contact_id]
    profile_json = params[:profile_json]

    @user = User.find_by(portico_id: contact_id)

    # default output value is an error, unless the following works
    output = ({error: true}.to_json)

    unless @user.nil? || profile_json.nil?
      campdoc_profile_id = CampdocService.get_campdoc_profile_id(@user)

      # add the profile ID to the user if we can
      unless (@user.campdoc_profile_id == campdoc_profile_id) || (campdoc_profile_id.nil?)
        @user.update_attributes(campdoc_profile_id: campdoc_profile_id)
      end

      # Create a profile if we didn't find one with our initial check above
      if campdoc_profile_id.nil?
        profile_data_hash = JSON.parse(profile_json)
        campdoc_profile_id = CampdocService.create_campdoc_profile(profile_data_hash)

        @user.update_attributes(campdoc_profile_id: campdoc_profile_id) unless campdoc_profile_id.nil?
      end

      output = ({id: profile_id}.to_json)

      # iterate through this user's related Porrtal profiles + link them in
      # CampDoc if possible
      @user.profiles.each do |profile|
        CampdocService.associate profile, @user
      end
    end

    render text: output
  end


  # GET /campdoc/profiles/completed (API)
  # returns TSV-delimited list of contact IDs and names of folks who've 100%
  # completed their campdoc profile
  def completed
    completed_campdoc_profiles = CampdocService.get_completed_campdoc_profiles

    if campdoc_completed_profiles.nil?
      output = '{"error":"true"}'
    else
      # render as tab-separated text
      output = "contact_id\tfirst_name\tlast_name\tdob\n"
      output += completed_profiles.map{|cp| "#{cp['identifier']}\t#{cp['givenName']}\t#{cp['familyName']}\t#{cp['dob']}"}.join("\n")
    end

    render text: output, content_type: 'text/plain'
  end 


  # DELETE campdoc/profiles/1
  # OR: POST campdoc/profiles/1/destroy (because FileMaker can't handle DELETE
  # requests)
  # expects parameters:
  # campdoc_profile_id (passed as :id), campdoc_group_id
  def destroy
    campdoc_profile_id = params[:id]
    campdoc_group_id = params[:campdoc_group_id]
    registration_id = nil
    output = {status: "error"}.to_json

    # Test for proper params
    if campdoc_profile_id.nil? || campdoc_group_id.nil?
      render_api_error "Invalid profile ID or group ID"
      return
    end

    # STEP 1: find the registration ID that matches the passed campdoc_group_id
    # (if any)
    registrations = CampdocService.get_registrations(campdoc_profile_id)

    if registrations.nil?
      render_api_error "Problem finding registrations for the given profile ID: #{campdoc_profile_id}"
      return
    end

    registrations.each do |reg|
      registration_id = reg['id'] if reg['groupID'].to_s == campdoc_group_id
    end

    if registration_id.nil?
      render_api_error "Profile #{campdoc_profile_id} is not registered into group #{campdoc_group_id}: #{registrations.inspect}"
      return
    end

    # STEP 2: delete that registration
    response_json = CampdocService.destroy_registration(campdoc_profile_id, registration_id)
    if response_json.nil?
      render_api_error "Couldn't de-activate registration id: #{registration_id}"
      return
    else
      # Successful delete, 204 means "success, no info necessary"
      output = {status: "Success: Registration #{registration_id} de-activated"}.to_json
    end

    render text: output, content_type: 'text/plain'
  end
  
end
