class Campdoc::SsoController < ApplicationController
  # GET /profiles/1/campdoc_sso
  def create
    @profile = Profile.find(params[:id])
    check_valid_profile @current_profile, @profile

    sso_url = CampdocService.get_sso_url(@profile.try(:email))

    if sso_url.nil?
      redirect_to dashboard_index_url, notice: "CampDoc single sign-on failed."
    else
      redirect_to sso_url
    end
  end
end
