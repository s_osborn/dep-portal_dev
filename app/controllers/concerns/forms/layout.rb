module Concerns
  module Forms
    module Layout
      extend ActiveSupport::Concern
      include Concerns::Common::DateVariables

      included do
        before_action :set_form_lists
        layout 'form'
      end

      def set_form_lists
        all_reqs = @current_user.requirements.sort_by { |s| "#{s.requirement_def.sort_order}#{s.requirement_def.name}" }
        @required_forms = []
        @medical_forms = []
        @optional_forms = []
        @trip_waivers = []

        all_reqs.each do |req|
          @required_forms << req if req.is_required
          @medical_forms << req if req.requirement_def.category == "Medical"
          @optional_forms << req if req.requirement_def.category == "Optional"
          @trip_waivers << req if (req.requirement_def.category == "Waiver" && !req.is_required)
        end

        @medical_forms_complete = true if @medical_forms.first && @medical_forms.first.is_complete
      end

      # GET /form_name/1
      # ALWAYS redirects to the forms_home url
      def show
        redirect_to forms_home_url
      end



      private

      # template method because the "Edit" function is basically the same in
      # all forms
      def do_edit(model)
        # meta-variables
        controller = eval("#{model.upcase_first.gsub(/_([a-z])/){|s| $1.upcase}}Form")
        form = "@#{model}_form"

        # grab the associated form requirement first
        @requirement = Requirement.find_by_id(params[:id]) || Requirement.new
        check_requirement_allowed(@requirement, @current_user)

        # this will set a view variable eg "@ambassador_form" or "@course_form"
        instance_variable_set form, 
            controller.method(:find_by).call(requirement_id: @requirement.id) 

        # if there is no form yet, create it 
        if (eval form + ".nil?")
          eval form + " = controller.method(:new).call({requirement_id: params[:id]})"
          # we pre-create forms, but can't validate them yet because they are empty
          eval form + ".save :validate => false"
        end

        # this variable is used in our views to disable form controls once the
        # form has been submitted
        @requirement.is_complete ? @disabled = true : @disabled = false
      end

    end
  end
end
