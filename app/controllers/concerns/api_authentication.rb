module Concerns
  module ApiAuthentication
    extend ActiveSupport::Concern

    # Check that the API token has been successfully passed as
    # params[:portal_token], or is in the session
    def check_api_auth
      unless api_auth(params[:portal_token]) || api_auth(session_api_token)
        render :json => "{'auth':'error'}"
        return
      end
    end

    def render_api_error(message = nil)
      render text: ({error: message}.to_json), content_type: 'text/plain'
    end
    
    private

    APITOKEN = 'dd674122358db45f7a1f76e11328ed20'

    # Extremely robust + secure API check
    def api_auth(token)
      token == APITOKEN
    end

  end
end
