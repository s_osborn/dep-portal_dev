module Concerns
  module Common
    module DateVariables
      extend ActiveSupport::Concern
      
      # This module contains instance variables + other config that are necessary
      # in *every* non-API view in the Portal

      included do
        before_action :set_current_year
        before_action :set_days_until_start
      end

      private

      # Show the current school year + calc the # of days left until start of
      # session
      def set_current_year
        today = Date.today
        @current_year ||= today.month > 8 ? today.year + 1 : today.year
      end

      def set_days_until_start
        today = Date.today

        # Loop through each possible start date (in chronological order), and
        # choose the earliest one
        start_date_fields = %w{w1_arrival_date s1_arrival_date w2_arrival_date w3_arrival_date w4_arrival_date s2_arrival_date w5_arrival_date w6_arrival_date}

        # Find the first user date that matches, or use today for start_date
        start_date = session_user.get_key(start_date_fields.find{|sdf|
          session_user.get_key(sdf) != nil
        }).try(:to_date) || today

        if (today > start_date)
          @days_left = nil
        else
          @days_left = (start_date - today).to_i.abs
        end
      end

    end
  end
end
