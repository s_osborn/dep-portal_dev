module Concerns
  module RedirectUtils
    extend ActiveSupport::Concern

    # Return the most recent request
    def back
      session_backpath || dashboard_index_url
    end

    # Validate that the user is an administrator
    def check_admin
      unless (session_is_admin == true || session_profile.is_admin)
        redirect_to root_url 
      end
    end

    # Validate that the form user is the same as the (current) user
    def check_form_allowed(form, user)
      redirect_to(root_url, :notice => "invalid form") unless(form && form.requirement.user_id == user.id)
    end

    # Make sure that the form user is the same as the current user
    def check_requirement_allowed(requirement, user)
      redirect_to(root_url, :notice => "invalid form") unless(requirement && requirement.user_id == user.id)
    end

    # Validate that the user is logged in, set some environment instance
    # variables which will be used in many many views
    def check_user_login
      set_current_profile
      set_current_user

      if @current_profile.nil?
        redirect_to login_url
      elsif @current_user.nil?
        redirect_to select_user_url
      end
    end

    # Make sure that the passed profile == the currently-logged-in profile
    def check_valid_profile(current_profile, found_profile)
      if current_profile.try(:id) != found_profile.try(:id)
        logger.warn "WARNING: Attempt made to access an invalid profile." + 
            "Logged-in: #{current_profile.inspect}" + 
            "Attempted Profile: #{found_profile.inspect}"
        redirect_to root_url 
      end
    end

    # often we need to redirect to a path, or first select a user then redirect
    # to a path
    def redirect_or_select_user(path, options)
      if session_user.nil? || session_user.try(:profile).try(:users).try(:count) == 1
        redirect_to select_user_url, options
      else
        redirect_to path, options
      end
    end

    # Remember the current request
    def remember_location
      set_session_backpath request.fullpath
    end

  end
end
