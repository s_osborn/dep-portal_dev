module Concerns
  module Dashboard
    module Layout
      extend ActiveSupport::Concern
      include Concerns::Common::DateVariables

      included do
        before_action :set_requirement_totals

        layout 'dashboard'
      end

      private
      # for the main dashboard view, we need to know how many forms are required
      def set_requirement_totals
        @total_requirements = 0
        @completed_requirements = 0
        session_user.requirements.each do |requirement|
          @completed_requirements += 1 if (requirement.is_complete && requirement.is_required)
          @total_requirements += 1 if requirement.is_required
        end
      end

    end
  end
end
