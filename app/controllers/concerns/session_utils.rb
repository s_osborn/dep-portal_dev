module Concerns
  module SessionUtils
    extend ActiveSupport::Concern

    # This module defines convenience/isolation methods for accessing session
    # variables + objects. By using these methods instead of accessing the
    # session directly, we save ourselves from ourselves by not breaking the
    # system if we decide to change how something is stored in the session.

    def session_api_token
      session[:api_token]
    end

    def session_is_admin
      session[:is_admin]
    end

    def session_backpath
      session[:back_path]
    end

    def session_profile
      Profile.find_by id: session[:profile_id]
    end

    def session_profile_id
      session[:profile_id]
    end

    def session_user
      User.find_by id: session[:user_id]
    end

    def session_user_id
      session[:user_id]
    end

    def set_current_profile
      @current_profile = session_profile
    end

    def set_current_user
      @current_user = session_user
    end

    def set_session_is_admin(bool=false)
      session[:is_admin] = bool
    end

    def set_session_api_token(token)
      session[:api_token] = token
    end

    def set_session_backpath(path)
      session[:back_path] = path
    end

    # This method expects a full Profile object
    def set_session_profile(profile)
      session[:profile_id] = profile.try(:id)
    end

    # This method expects a full User object
    def set_session_user(user)
      session[:user_id] = user.try(:id)
    end
  end
end
