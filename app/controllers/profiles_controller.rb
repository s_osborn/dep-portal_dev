class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :login_as, :reset_password, :resend_welcome, :update, :destroy]

  layout 'basic'

  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @profiles }
    end
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @profile }
    end
  end

  # GET /profiles/new
  # GET /profiles/new.json
  def new
    @profile = Profile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @profile }
    end
  end

  # GET /profiles/1/edit
  def edit
  end

  # GET /profiles/1/login_as
  def login_as
    set_session_profile @profile
    set_session_user(User.find_by(id: params[:user_id]))
    
    redirect_or_select_user root_url, notice: "logged in"
  end

  # GET /profiles/1/reset_password
  def reset_password
    set_session_profile @profile
    set_session_user nil

    redirect_to reset_password_url
  end

  # GET /profiles/1/resend_welcome
  def resend_welcome
    @user = User.find params[:user_id] || @profile.users.first
    if @profile
      Notifier.welcome_email(@profile, @user, @profile.add_token).deliver
      redirect_to users_url, notice: "Welcome email sent"
    else
      redirect_to users_url, notice: "Profile not found"
    end
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_or_select root_url, notice: 'Profile was successfully created.' }
        format.json { render json: @profile, status: :created, location: @profile }
      else
        format.html { redirect_to login_url, notice: "Password update failure" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /profiles/1
  # PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update_attributes(profile_params)
        format.html { redirect_or_select_user root_url, notice: 'Profile was successfully updated.' }
      else
        format.html { redirect_to login_url, notice: "Password update failure" }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy

    respond_to do |format|
      format.html { redirect_to profiles_url }
      format.json { head :no_content }
    end
  end



  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:email, :full_name, :password, :password_confirmation)
  end
end
