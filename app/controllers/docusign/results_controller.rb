class Docusign::ResultsController < ApplicationController

  # GET /docusign/results/:id
  def show
    @requirement_def = RequirementDef.find_by(short_code:params[:form_code])

    if params[:user_id]
      user = User.find_by(id: params[:user_id])
      # if a family is getting fancy and doing DocuSign for multiple users at
      # once, change the currently-selected user to the passed user
      set_session_user(user) unless current_profile.users.find_by(id: user.id).nil?
    end

    if params[:event] == "signing_complete"
      @requirement = user.requirements.find_by(requirement_def_id: @requirement_def.id)
        # TODO? perhaps store the PDF url of the result? Though that will automatically get emailed to us.
      @requirement.update_attributes(is_complete: true) unless @requirement.nil?
      flash[:notice] = "Thanks! Your #{@requirement_def.try(:name)} form has been successfully signed."
    else
      flash[:notice] = "You chose not to finish signing your #{form_name} form."
      logger.warn "Docusign form result not successfully signed: #{params.inspect}"
    end

    redirect_to forms_home_path
  end

end
