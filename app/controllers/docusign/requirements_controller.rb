class Docusign::RequirementsController < ApplicationController
  before_action :get_requirement

  # DELETE docusign/requirements/1
  # removes a docusignURL
  def destroy
    docusign_url = DocusignUrl.find_by(requirement_id: @requirement.id, profile_id: @current_profile.id)

    if @requirement.nil? || @requirement.try(:requirement_def).try(:docusign_template_id).nil?
      redirect_to forms_home_path, alert: "DocuSign template not found."
    elsif docusign_url
      docusign_url.destroy
      @requirement.update_attributes({
          is_complete: false,
          is_exported: false
      })
      redirect_to forms_home_path, notice: "DocuSign form reset"
    end
  end

  # GET docusign/requirements/1
  # this redirects you to the URL of the docusign form associated with the
  # passed requirement
  def show
    docusign_url = DocusignUrl.find_by(requirement_id: @requirement.id, profile_id: @current_profile.id)
    return_url = docusign_result_url(form_code: @requirement.try(:requirement_def).try(:short_code), user_id: @current_user.id)

    if @requirement.nil? || @requirement.try(:requirement_def).try(:docusign_template_id).nil?
      redirect_to forms_home_path, alert: "DocuSign template not found."
    elsif docusign_url
      # use the envelope we've already generated to get us an updated URL
      @url, envelope_response = DocusignService.get_form_url(docusign_url, @current_profile, return_url)

      # for various reasons, we end up storing DocuSign URLS that are no longer
      # valid. In this case we want to remove them from the system + try again
      if @url.nil?
        docusign_url.destroy
        redirect_to docusign_requirement_path(@requirement.id)
      else
        docusign_url.update_attributes(url: @url)
        redirect_to docusign_url.url, status: 302 #found
      end
    else
      # else generate a new envelope, and new URL, and go to it
      @url, @envelope_response = DocusignService.create_form_url(@current_profile, @current_user, @requirement, return_url)
      logger.info

      if @url
        DocusignUrl.create(
          requirement_id: @requirement.id, 
          profile_id: @current_profile.id, 
          docusign_envelope_id: @envelope_response["envelopeId"],
          url: @url['url']
        )
        redirect_to @url, status: 302 #found
      else
        redirect_to forms_home_url, notice: "Could not create Docusign template link."
      end
    end
  end

  
  private

  def get_requirement
    @requirement = Requirement.find_by(id: params[:id])
  end

end
