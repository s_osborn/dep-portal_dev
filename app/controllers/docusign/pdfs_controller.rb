class Docusign::PdfsController < ApplicationController
  before_action :check_api_auth
  skip_before_action :check_user_login

  # API-only
  # GET /docusign/pdfs/1
  def show
    @requirement = Requirement.find_by(id: params[:id])

    if @requirement.nil? || @requirement.try(:requirement_def).try(:docusign_template_id).nil?
      render text: 'error: no requirement or template'
    else
      docusign_url = nil

      @requirement.try(:user).try(:profiles).try(:each) do |p|
        url = p.docusign_urls.find_by(requirement_id: requirement.id)
        unless url.class == NilClass
          docusign_url = url 
          break
        end
      end

      if docusign_url
        file_path = "#{Rails.root.join('tmp/docusign/')}#{rand(36**8).to_s(36)}.pdf"
        DocusignService.download_pdf(docusign_url, file_path)
        send_file file_path, filename: "#{@requirement.requirement_def.name}.pdf", type: 'application/pdf'
      else
        render text: 'error: no docusign URL'
      end
    end
  end
end
