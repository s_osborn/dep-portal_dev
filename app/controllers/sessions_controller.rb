class SessionsController < ApplicationController
  skip_before_action :check_user_login, only: [:new, :create, :create_from_email, :destroy, :forgot_password, :maintenance, :reset_password, :select_user]
  
  layout 'basic'

  # Login form
  def new
  end

  # Profile tries to log in
  def create
    profile = Profile.find_by(email: params[:email])
    if profile.try(:authenticate, params[:password])
      set_session_profile profile
      set_session_is_admin(true) if profile.is_admin

      if profile.try(:users).try(:count) == 1
        set_session_user profile.users.first
      end

      redirect_or_select_user back, :notice => "logged in"
    else
      redirect_to login_url, :alert => "Your password is incorrect or your email address is not in our system"
    end
  end

  # User logs in from email link
  def create_from_email
    token = Token.find_by(token: params[:token])
    
    if token.nil? || token.try(:profile).nil?
      redirect_to login_url, :alert => "Invalid link."
    else
      profile = token.profile 
      set_session_profile profile

      if profile.try(:password).nil?
        redirect_to reset_password_url, :notice => "Please set your password now. Your email link to log in will expire shortly, and you will need a password to log in from that point on."
      else
        redirect_or_select root_url, :notice => "Successfully logged in"
      end
    end
  end

  # User logs out
  def destroy
    set_session_profile nil
    set_session_user nil
    set_session_is_admin nil

    redirect_to login_url, :notice => "Logged out!"
  end

  # User forgot password but has an email. Generate a secure login token + send
  # it along.
  def forgot_password
    if request.post?
      profile = Profile.find_by(email: params[:email])
      # Generate a new security token for this user.
      if profile
        token = profile.add_token
        # Now deliver the forgotten password email
        Notifier.forgot_password(profile, token).deliver
        redirect_to login_url, :notice => "Forgot password email sent"
      else
        redirect_to login_url, :alert => "Error - email not found"
      end
    end
  end

  def maintenance
    @message = "We are currently under maintenance. Please try back later"
  end

  # you get redirected here from the email link 
  def reset_password
    token = Token.find_by(token: params[:token])
    profile = token.try(:profile)
    set_session_profile(profile) unless profile.nil?

    if session_profile.nil?
      redirect_to root_url, :alert => "Invalid information provided"
    else
      # we (might) need to set the @current_profile instance variable manually
      # here in the event that we skipped the login check.
      set_current_profile
      # API token needs to be set for User update to work
      # TODO: this is a hack. make it not a hack.
      set_session_api_token ApplicationController::APITOKEN
    end
  end

  # if an account is linked to many students, you need to select the currect one
  def select_user
    # We can't check that the user is logged in here, because it'll create a
    # redirect loop. So we check login manually.
    redirect_to(root_url) if session_profile_id.nil?

    @users = session_profile.try(:users)
    @users = @users.try(:sort_by) { |s| "#{s.get_key('last_name')}#{s.get_key('first_name')}" }.try(:uniq)

    if params[:user_id]
      user = User.find_by(id: params[:user_id])
      set_session_user(user) if user
      redirect_to root_url
    end
  end
end
