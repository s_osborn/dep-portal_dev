class Users::ProfileAssociationsController < ApplicationController
  include Concerns::Dashboard::Layout

  # GET /users/1/profile_association/new
  def new
  end

  # POST /user/1/profile_association
  def create
    email = params[:email]
    full_name = params[:full_name]

    @profile = Profile.find_by_email(email)

    #If the profile exists, update it to include the user
    if @profile.nil?
      #profile is new
      @profile = Profile.new(
        email: email,
        full_name: full_name,
        password: SecureRandom.hex(32)
      )
      @profile.save(validate: false)
      @profile.users << @current_user
      Notifier.welcome_email(@profile, @current_user, @profile.add_token).deliver
    else
      unless @profile.users.find_by(id: @current_user.id)
        @profile.users << @current_user 
        Notifier.add_profile(@profile, @current_user, @current_profile.full_name).deliver
      end
    end

    # make sure to associate the found profile with the user in campdoc
    CampdocService.associate @profile, @current_user

    redirect_to forms_home_path, notice: "#{full_name} has been added to #{@current_user.get_key('first_name')}'s account."
  end
  
end
