class Flights::LookupsController < ApplicationController
  # POST /flights/lookup
  # "create" a flight lookup eg. look up flight information. This only responds
  # to AJAX calls and thus only returns a JS snippet.
  def create
    @airline = params[:airline]
    @airport = params[:airport]
    @flightno = params[:flightno]
    @date = params[:date].to_date
    @method = params[:method]

    if @method == "arrival"
      @results = FlightLookupService.schedule_lookup_arriving(@airline, @flightno, @date.year, @date.month, @date.day)
    elsif @method == "departure"
      @results = FlightLookupService.schedule_lookup_departing(@airline, @flightno, @date.year, @date.month, @date.day)
    end

    @html = "<h3>Flight Validation</h3><p>Please select the correct flight from the list below. If you do not see the correct flight, please rekey your flight information above and refresh the flight list.</p><table class='bg-white'><thead><tr><th class='pa2'>Flight #&nbsp;</th><th class='tl pa2'>Time</th><th></th></tr></thead><tbody>"

    if @results.nil?
      @html = "<p class='emphasize'>NO MATCHING FLIGHTS FOUND</p>"
    else
      @results.each do |result|
        unless result.arrival_time.empty? && result.departure_time.empty?
          @the_time = Time.parse(result.arrival_time) if @method == "arrival"
          @the_time = Time.parse(result.departure_time) if @method == "departure"
          @display_time = "#{@date.strftime('%m/%d')} - #{@the_time.strftime('%I:%M%p')}"
          @arrival_hour = @the_time.strftime("%I")
          @arrival_time = @the_time.strftime("%I:%M")
          @am_pm = @the_time.strftime("%p")
          @flight_number = result.flight_number
          @html += "<tr><td class='pa2'>#{@flight_number}</td><td data-arrival-time='#{@arrival_time}' data-arrival-hour='#{@arrival_hour}' data-am-pm='#{@am_pm}' class='pa2'>#{@display_time}</td><td class='select-this-flight pa2 link x-orange x-hover-darkorange pointer')}'>select</td></tr>"
        end
      end
      @html += "</tbody></table>"
    end

    respond_to do |format|
      format.js
    end
  end
end
