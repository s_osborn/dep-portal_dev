class RequirementsController < ApplicationController
  include Concerns::Forms::Layout

  def index
  end

  # PUT/PATCH /requirements/1
  # currently, we only use this to toggle requirement completion. So it also
  # expects a "toggle" param. You can send anything as the "toggle" value -
  # it'll all evaluate to truthy
  def update
    @requirement = Requirement.find_by(id: params[:id])

    respond_to do |format|
      if (
        params[:toggle] &&
        @requirement.try(:update_attributes, {
          is_complete: !@requirement.is_complete,
          is_exported: false
        })
      )
        format.html { redirect_to forms_home_url, notice: 'Requirement updated' }
      else
        format.html { redirect_to forms_home_url, notice: 'Requirement update failure' }
      end
    end
  end
end
