class Admin::AirlinesController < ApplicationController
  autocomplete :airline, :name, :display_value => :display_name, :extra_data => [:icao]#, :full => true
  before_action :check_admin, except: [:autocomplete_airline_name]
  before_action :set_airline, only: [:show, :edit, :update, :destroy]
  layout 'basic'

  # GET /airlines
  # GET /airlines.json
  def index
    @airlines = Airline.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @airlines }
    end
  end

  # GET /airlines/1
  # GET /airlines/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @airline }
    end
  end

  # GET /airlines/new
  # GET /airlines/new.json
  def new
    @airline = Airline.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @airline }
    end
  end

  # GET /airlines/1/edit
  def edit
  end

  # POST /airlines
  # POST /airlines.json
  def create
    @airline = Airline.new(airline_params)

    respond_to do |format|
      if @airline.save
        format.html { redirect_to airline_url(@airline), notice: 'Airline was successfully created.' }
        format.json { render json: @airline, status: :created, location: @airline }
      else
        format.html { render action: "new" }
        format.json { render json: @airline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /airlines/1
  # PUT /airlines/1.json
  def update
    respond_to do |format|
      if @airline.update_attributes(airline_params)
        format.html { redirect_to airline_url(@airline), notice: 'Airline was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @airline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /airlines/1
  # DELETE /airlines/1.json
  def destroy
    @airline.destroy

    respond_to do |format|
      format.html { redirect_to airlines_url }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_airline
    @airline = Airline.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def airline_params
    params.require(:airline).permit(:icao, :short_name, :name, :callsign, :country, :url, :location)
  end
end
