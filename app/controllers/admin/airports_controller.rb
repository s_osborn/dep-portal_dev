class Admin::AirportsController < ApplicationController
  autocomplete :airport, :name, :display_value => :display_name, :extra_data => [:icao], :full => true

  before_action :check_admin, except: [:autocomplete_airport_name]
  before_action :set_airport, only: [:show, :edit, :update, :destroy]

  layout 'basic'


  # GET /airports
  # GET /airports.json
  def index
    @airports = Airport.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @airports }
    end
  end

  # GET /airports/1
  # GET /airports/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @airport }
    end
  end

  # GET /airports/new
  # GET /airports/new.json
  def new
    @airport = Airport.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @airport }
    end
  end

  # GET /airports/1/edit
  def edit
  end

  # POST /airports
  # POST /airports.json
  def create
    @airport = Airport.new(airport_params)

    respond_to do |format|
      if @airport.save
        format.html { redirect_to airport_url(@airport), notice: 'Airport was successfully created.' }
        format.json { render json: @airport, status: :created, location: @airport }
      else
        format.html { render action: "new" }
        format.json { render json: @airport.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /airports/1
  # PUT /airports/1.json
  def update
    respond_to do |format|
      if @airport.update_attributes(airport_params)
        format.html { redirect_to airport_url(@airport), notice: 'Airport was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @airport.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /airports/1
  # DELETE /airports/1.json
  def destroy
    @airport.destroy

    respond_to do |format|
      format.html { redirect_to airports_url }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_airport
    @airport = Airport.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def airport_params
    params.require(:airport).permit(:icao, :latitude, :location, :longitude, :name, :timezone)
  end
end
