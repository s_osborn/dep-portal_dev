class Admin::RequirementDefsController < ApplicationController
  before_action :set_requirement_def, only: [:show, :edit, :update, :destroy] 
  before_action :check_admin
  layout 'basic'

  # GET /requirement_defs
  # GET /requirement_defs.json
  def index
    @requirement_defs = RequirementDef.order(:category,:sort_order)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @requirement_defs }
    end
  end

  # GET /requirement_defs/1
  # GET /requirement_defs/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @requirement_def }
    end
  end

  # GET /requirement_defs/new
  # GET /requirement_defs/new.json
  def new
    @requirement_def = RequirementDef.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @requirement_def }
    end
  end

  # GET /requirement_defs/1/edit
  def edit
  end

  # POST /requirement_defs
  # POST /requirement_defs.json
  def create
    @requirement_def = RequirementDef.new(requirement_def_params)

    respond_to do |format|
      if @requirement_def.save
        format.html { redirect_to requirement_def_url(@requirement_def), notice: 'Requirement def was successfully created.' }
        format.json { render json: @requirement_def, status: :created, location: @requirement_def }
      else
        format.html { render action: "new" }
        format.json { render json: @requirement_def.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /requirement_defs/1
  # PUT /requirement_defs/1.json
  def update
    respond_to do |format|
      if @requirement_def.update_attributes(requirement_def_params)
        format.html { redirect_to requirement_def_url(@requirement_def), notice: 'Requirement def was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @requirement_def.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requirement_defs/1
  # DELETE /requirement_defs/1.json
  def destroy
    @requirement_def.destroy

    respond_to do |format|
      format.html { redirect_to requirement_defs_url }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_requirement_def
    @requirement_def = RequirementDef.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def requirement_def_params
    params.require(:requirement_def).permit(:action, :category, :docusign_template_id, :name, :requirement_type, :short_code, :sort_order)
  end
end
