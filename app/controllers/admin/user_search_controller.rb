class Admin::UserSearchController < ApplicationController
  before_action :check_admin

  layout 'basic'

  # GET /admin/user_search/
  def index
    @users = []
  end

  # POST /admin/user_search
  def create
    query = params[:query]
    reg_query = Regexp.quote query

    @users = []
    User.all.each do |user|
      @users << user if (user.information.match(/#{reg_query}/i))
    end

    Profile.all.each do |profile|
      profile.users.each{|u|@users << u} if (profile.email.match(/#{reg_query}/i))
    end

    @users = @users.sort_by { |s| "#{s.get_key('last_name')}#{s.get_key('first_name')}" }.uniq
    
    respond_to do |format|
      format.js { render :javascript => @users }
    end

  end
end
