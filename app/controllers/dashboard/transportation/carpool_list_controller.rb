class Dashboard::Transportation::CarpoolListController < ApplicationController
  layout 'basic'

  # show the list of people who want to carpool
  def index
    @sessions = @current_user.enrolled_sessions
    @bus_route = @current_user.get_key('bus_route')
    @bus_stop = @current_user.get_key('bus_number')
    @bus_stop_notes = @current_user.get_key('bus_stop_notes')
    @bus_driver = @current_user.get_key('bus_driver')

    @sessions.each do |session|
      User.all.each do |u|
        session.user_array << u if (u.enrolled_in(session) && u.match('carpool_list','1'))
      end
      session.user_array = session.user_array.sort_by { |x| "#{x.get_key('zip') || '___'} #{x.get_key('first_name')}" }
    end
  end
end
