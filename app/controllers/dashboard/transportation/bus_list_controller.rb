class Dashboard::Transportation::BusListController < ApplicationController
  layout 'basic'

  # show the list of bus people on the same route
  def index
    @sessions = @current_user.enrolled_sessions
    @bus_route = @current_user.get_key('bus_route')
    @bus_stop = @current_user.get_key('bus_number')
    @bus_stop_notes = @current_user.get_key('bus_stop_notes')
    @bus_driver = @current_user.get_key('bus_driver')

    @sessions.each do |session|
      User.all.each do |u|
        session.user_array << u if (u.enrolled_in(session) && u.match('bus_route', @bus_route))
      end
      session.user_array.sort! { |x,y| x.get_key('bus_number') <=> y.get_key('bus_number') }
    end
  end

end

