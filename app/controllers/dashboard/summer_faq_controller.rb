class Dashboard::SummerFaqController < ApplicationController
  include Concerns::Dashboard::Layout

  def index
    @enrolled_campuses = @current_user.enrolled_sessions.map{|s|s.campus}.uniq
    @enrolled_campuses ||= [false]
  end 
end

